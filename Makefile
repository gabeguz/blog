build: 
	hugo
serve:
	hugo server --buildDrafts --buildExpired --buildFuture
deploy:
	rsync -rlptDvz --delete public/ root@www:/var/www/htdocs/www.lifewaza.com/
