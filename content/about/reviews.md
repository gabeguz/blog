+++
title = "About reviews on this site"
description = "Sometimes, I review products I buy. My rating scale is simple, would I buy it again, yes or no."
date = "2020-11-02"
+++

Sometimes I review products that I buy. My rating scale is simple: 

Would I buy this again? 

- Yes
- No

Links to products in my reviews are generally referral links. This gives me a kickback if you go on to purchase something I've reviewed. 

Everything I review is something I use.

You can find all my [reviews here](/categories/reviews).
