+++
description = "An apology to my blog for not writing."
categories = [
  "random",
]
tags = [
  "letter",
  "dearblog",
  "apology",
]
date = "2006-03-22T00:00:43-05:00"
title = "An open letter to my blog"

+++
Dear Blog,

I apologize profusely for not directing any creative juices in your general
direction. What can I say, there are times when the happenings of daily life
adjust the priorities of duties and activities. I realize that I should spend
more time fine tuning my writing skills, an activity to which you are well
suited. I also see that allowing you to stagnate reflects poorly on my own
character, and that is unfortunate.

It may please you to know that I recently visited several dojos in and around
the Bay Area, and had an excellent time training with a variety of Aikido
teachers. My friend and former roommate Greg, who was on his way to
philadelphia to accept a Teach For America position, accompanied me. After our
brief stint in the Bay Area, we went to Yosemite for a short, but extremely
fulfilling day hike up to Nevada and Vernal falls along the very aptly named
Mist Trail.

Naturally, I took several pictures to catalog my adventures, and they are, as
usual posted in my gallery. Perhaps viewing them will ameliorate, to some
degree, the feelings of neglect I have caused.

I remain your friend and confidant,

Gabriel Guzman
