+++
title = "Ansible"
categories = [ "tech" ]
tags = [ "OpenBSD", "UNIX", "ansible" ]
date = "2021-04-13"
+++

I've been playing with ansible off and on for several years now, usually in fits
and starts. Ansible is a tool for managing computers. It lets you do things like
setup a new web server in *exactly* the same way you setup your old web server, or
make sure that your new laptop has all the software you routinely use installed,
or configure a cluster of computers identically regardless of their underlying
operating system, package manager, or authentication system.

In short, it's a super powerful tool for automating everything about all your
computers.

Every time I manually provision a new server I tell myself, "this time I'm going
to use ansible", and every time I start I end up getting frustrated that it's so
hard to get started using ansible and I just provision the sever manually,
again. Finally, I found a course on pluralsight (thanks $EMPLOYER) that walks
you through setting up ansible to manage just your local machine. This was the
guide I needed. It gave simple instructions that clearly told you how to get
ansible working for just this one machine, from there it was pretty easy to
apply the same learnings to several machines and I'm now happy to say that I'm
using ansible to manage:

- my work laptop
- my personal laptop
- my home router/firewall

This means that if I ever need to replace my router, I can just run the router
playbook that I've created on the new machine and my router will be provisioned
exactly the same way as my current router. It also means that if one of my
laptops kicks the bucket, I can be up and running again on a new machine
quickly. I won't need to remember all the stuff I usually have to install, I
won't need to remember the special way I have to configure my local smtp server
to relay outgoing mail to my real smtp server. I won't have to spend time trying
to get my firewall rules just right. As long as I keep only using ansible to
manage these machines, I'll have no snowflakes!

It also means that I can reuse the work I've done to get these machines setup to
get my other machines setup as well. The ones that are left to do are:

- smtpd server
- httpd server
- minecraft server
- vpn server

Each of those has their own unique tweaks that I'll have to port to ansible
before I can fully use it to provision them, but there will also be a fair
amount of overlap from the work I've already done.

If you have a pluralsight license, I can happily recommend this course: [Getting
started with
ansible](https://www.pluralsight.com/courses/getting-started-ansible) and if you
don't [here's a blog
post](https://blog.laurentcharignon.com/post/laptop-setup-with-ansible/) that
takes a similar approach (i.e. managing your personal laptop with ansible).
Finally, [the ansible
docs](https://docs.ansible.com/ansible-core/2.10/user_guide/index.html) are a useful if daunting resource.
