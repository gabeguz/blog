+++
tags = [
  "travel",
  "armenia",
  "karabagh",
  "adventure",
]
description = "A story about my trip to Karabagh"
date = "2005-09-11T23:11:08-05:00"
title = "Armenia - Karabagh"
categories = [
  "adventure",
]

+++
 Picture of the statue of Mamik yev Babik, in KarabaghOn my first weekend in Armenia, I was privileged to join a group of students from Yerevan State University on a trip to Karabagh. I am incredibly thankful to Prof. Yeranosyan for allowing me to join him and his students on this fascinating adventure. The following story is based on that trip.

By 7:00 am friday morning we were ready to go. Gor and I met Constantine (Cot) and his girlfriend Tanya (Tan) downstairs and split a cab to the University. We arrived first though more students trickled in slowly, as did Yeranosan, the organizer. A school bus and micro bus pulled up, setting Yeranosyan into action. He called everyone into a circle, read off our names, and organized us onto the two busses. As Lusine is good friends with Yeranosyan, my group, Gor, me, Cot, Tan, Lusine, and Nare, were ushered onto the microbus where we would spend the next several hours.

We left the University about 1/2 hour later than planned. I'm not saying it is good or bad, but, Armenians have the same view of punctuality as Mexicans. Armenia is a comparatively small country, about the size of Maryland, yet somehow it still took us all day to get to Karabagh. I'm not complaining, anyone who knows me, knows I'm all about the journey, and that the destination is secondary. This trip was no exception.

As the ride progressed, we moved into more and more mountainous terrain, stopping from time to time to get gas, eat food, allow the slower bus to catch up, and just take a break from sitting. I would have to endearingly call this the longest short trip I have ever been on.

As the hours ticked by, I was able to become better acquainted with the group of people who were to become my friends. Though I had met them all before, aside from Gor and Lusine, I hadn't spent much time with any of them. Nothing brings people together like being smashed together in a microbus for hours on end.

Hours and hours of bumping around, trying to stay comfortable while passing the time. To that end, I was introduced to a game called Contact. Maybe I will try to explain it one day, for now, you will just have to wonder. It was a pretty fun game, but after awhile, I just couldn't keep playing and so retreated into my book for a time. When I came out of my literary stupor, Nare and I began to talk.

Nare is an undergraduate Physics student at Yerevan State University and Lusine's best friend. She speaks Armenian and Russian fluently, her English is more than adequate, and she was studying Spanish. Spanish ended up being a focal point of our conversations. Nare is a very quick learner. She would help me with my Armenian, and I would answer her seemingly endless stream of questions on the Spanish language. By the end of my trip, I could count to ten in Armenian, while Nare was conjugating irregular verbs and forming complete sentences in Spanish. I would like to believe that Armenian is just much more difficult to learn than Spanish, or perhaps that I am a better teacher than Nare, but the truth is I am just not as good a student. Something to work on.

When the bus finally arrived in Stepanakert, the capital of Karabagh, everyone was fairly tired from the long drive. It took us a bit of extra driving to find the University that would be giving us a place to sleep, and after unpacking we decided to go out and explore the town a bit. Yeranosyan made it very clear that curfew was 10 p.m. and that the doors would be closed and locked at that time. This gave us about an hour on our own.

Being in Armenia, accompanied by Armenians, we naturally decided to find a place to get some coffee. After a short walk, we found a very green, smoke filled cafe about a quarter mile from the school we were staying at. The six of us sat down, ordered some food, coffee, and tea, and quickly whiled away our hour of free time. Naturally, we did not make it back to the University before 10. Instead, we all got a chance to practice our commando skills by sneaking our way back onto the campus and then trying to enter the presumably locked building.

We were able to get through the locked gate, onto the campus proper, and then walked around the main building, looking for a weak point in it's perimeter. On our second pass, Lucine decided to try a very unorthodox tactic. She knocked on the front door. A novel idea, for sure, but it paid off. One of the students who had made it back in time heard us, came upstairs and pulled open the door. The unlocked door. Wait a minute, why was the door unlocked? Why didn't we even bother to check? Everyone knows what happens when you assume, and this was no exception. Moral of the story? Before attempting to break in, make sure you are actually locked out.

When we got back to the concrete floored gym that was to be our room for the evening, people had spread their sleeping bags near the walls, and clustered into groups of 5 to 10 people, around the room. These groups were talking, sleeping, or singing to keep themselves entertained.

The group that was singing also had a small drum, and they sounded amazing. The acoustics of the room coupled with the fabulous voice of one of the students made for an almost concert hall experience. You could tell right away that the students were singing songs about their country, about who they were, songs that spoke to their hearts and souls. The emotion behind their singing was palpable and moving. Though I knew the answer, I still asked what the songs were about. Patriotic songs, songs of Hayastan (Armenia).

The singing continued for quite some time. I didn't mind at all. This was one of the singular experiences of my trip, something that I doubt I will experience in the same way again. At some point in the night, I believe Yeranosyan said something, and people began to go to sleep. The concrete floor was comfortable, as far as concrete floors go, and the temperature in the gym was conducive to a good nights rest. We awoke early the next morning, washed up, packed our bags, and shuffled back onto the busses.

We spent all of Saturday visiting and exploring the interesting sites in Karabagh. Demolished Azeri towns, ancient castles, monastery compounds, graves, and monuments. You name it, we saw it. Yeranosyan was an excellent guide, as were Lucine and Nare who both know quite a bit about the history of their country. Between the three of them, I learned more than I could possibly remember about the history or Armenia, for which I am very grateful.

That night, upon our return to the gymnasium, I started to feel very cold. At first, I didn't think anything of it, simply assuming it was chillier than the night before. Finally, when I reached for my wool cap to keep warm, I asked someone if it was cold out, or just me. Unfortunately, it was just me, and by now, I was freezing. T-shirt, sweatshirt, fleece, wool socks, wool cap, jacket, and still, I was cold.

My friends, being the excellent individuals that they are, put me in the warmest sleeping bag and made me some hot tea. Yeranosyan noticed me in my mummified state, which I will admit, must have looked quite funny, and said: "This isn't Siberia, you know". At this point, he went on to recount a story about how he once rode his bicycle from Yerevan to Moscow in the middle of winter. It was a good story, though it didn't help make me any warmer.

I was sick. Apart from the cold, I developed a low fever, and my stomach started to complain. I went to sleep. People later told me that the night was a repeat of the one before, with singing, drumming, and laughing. Somehow I managed to sleep through all of it. I wouldn't have thought it possible to sleep through all that, but apparently, I did. Around 2 in the morning, I awoke. I was hot, very hot. Why did I have all these clothes on? I peeled off every layer (well, almost every layer) and opened up the sleeping bag. Damn it was hot.

Again, I slept.

I awoke. Everything felt great. I wasn't hot, I wasn't cold. My stomach didn't hurt, things were back to normal. Yes, all the tiny cockroaches crawling around everywhere were normal too. It was Sunday, time to go home.

More pictures of Karabagh available in the gallery. 
