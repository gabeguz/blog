+++
categories = [
  "adventure",
]
tags = [
  "adventure",
  "armenia",
  "travel",
  "yerevan",
]
description = "My visit to Armenia"
date = "2005-10-24T23:01:23-05:00"
title = "Armenia - Yerevan"

+++
Yerevan is the capital city of Armenia. It is historically and culturally a
fascinating place. Some type of settlement has existed at the same location for
over 2787 years. While my wanderings were mostly confined to where I could get
on my own two feet (and microbus/metro/taxi), I was able to get an excellent
feel for the city, especially it's center.

The earliest record of settlement was found at the ruins of ancient Erebuni.
Cuneiform records found at the site of the ruins mark the castle as having been
built by Argistisi I in 782 c.e. with the help of 6,600 captured slaves.

Armenians are a proud people, and Yerevan reflects this. Galleries of Armenian
artists, monuments and statues dedicated to Armenian composers, authors,
architects, poets, and heros are everywhere. They are only outnumbered by cafes,
a situation I will speak to a bit later on. My favorite museum was the Museum of
Sergei Parajanov an amazing Armenian artist and filmmaker. Though I don't claim
to understand his most famous film, The Color of Pomegranates, it was definitely
a unique experience. If you get the chance, watch it.

The master architectural plan for modern day Yerevan was drawn up by Alexander
Tamanian from 1924 to 1926. As far as I understand, it consists of everything
within the park belt surrounding the inner part of the city. There were some
difficulties encountered when executing the plan due to rapid population growth
during the years Armenia was part of the Soviet Union, but overall the plan
appears to have been well carried out. I'm not an architect, so I don't know if
they make models for city scale projects, but if they do, the model for Yerevan
would be an awesome thing to behold.

One thing that some of the locals I spoke to were discomfited by was the number
of "modern" buildings now being built in Central Yerevan. Glass and concrete
towers are springing up like weeds, replacing older homes, and buildings. In my
mind, it should be possible to maintain a traditional look and feel while
modernizing the interior of these buildings. This would be the ideal, I think.
Some people didn't mind so much, but I find myself agreeing with the people who
find these new buildings annoying. They definitely don't blend well with the old
pink stone that makes up the majority of the older construction, and they are in
no way traditional.

A word or two on Cafes. They are everywhere. Literally. It is not an
exaggeration to say that it is difficult to find a block without at least one.
In some cases, rows of Cafes stretch entire blocks. I find it very difficult to
believe that even half of them can be profitable, but their abundance suggests
otherwise. The worst thing about several of these Cafes is that they have been
built over parts of the green park area that surrounds the inner part of the
city. In fact Cafes have totally supplanted park in several areas, which is
quite a shame. I enjoy Coffee as much as the next guy, but you can have too much
of a good thing.

Over the course of my stay in Yerevan, I couldn't help but be reminded of the
Mexico City of my childhood. Though Yerevan is nowhere near as populated as the
D.F., it had a very D.F. feel to me. No holds barred driving, mordidas (bribes),
and street vendors hawking everything from food to pirated software, it was nice
to feel so at home. There was even a tianguis (native market), called Vernisage
where locals sold everything from antique cameras to hand crafted woodwork.

Pedestrian life in Yerevan is anything but. While there are traffic lights, stop
signs, and crosswalks, it would be foolhardy to rely on them. Crossing the
street in Yerevan is mostly a question of faith. Faith in yourself, faith in the
drivers, and faith in God. Crossing a busy street is an especially exhilarating
experience which involves making it 1/2 way across and waiting on the dividing
line for your next opening. I couldn't help but picture myself as frogger, on
the last level, with only one life left. I was never really afraid of being hit,
but going for a walk definitely required a heightened level of awareness.

How was the night life? Well, if you know me, then you know I'm not really the
right guy to ask that question to, but I will do my best to answer. Yerevan
seems to be a pretty happening place, with a large youth population. Cafes and
bars are generally open very late, and it is not difficult to find places open
24 hours a day. I never made it to a night club, so I have no idea what they are
like. Perhaps that will be a question for my next visit, but don't hold your
breath.

What else can I say about Yerevan? It is a great and safe city. Excellent for
exploring, and there are few tourists. I saw very few travelers while I was
there, and only one of those was American. There is no McDonalds, no Burger
King, and the only Taco Bell was Takobell. The dollar is still relatively strong
in Armenia, and a small amount of money can go a long way. It is an ideal place
to visit if you are looking for something different and exciting, yet
comfortable and familiar at the same time. It is a city I hope to return to, and
recommend highly to anyone looking for their next vacation destination.
