+++
description = "A review of the book Asterisk, The Future of Telephony"
date = "2006-03-29T23:46:03-05:00"
title = "Asterisk, The Future of Telephony"
categories = [
  "tech",
]
tags = [
  "asterisk",
  "pbx",
]

+++

Asterisk
The Future of Telephony

By Jim Van Meggelen, Jared Smith, and Leif Madsen

ISBN: 0-596-00962-3, First Edition, 9/2005, $39.95, O'REILLY

Asterisk IS the future of telephony. I have been wanting to dive into the Asterisk world for over a year now, but never had the time until now. I have very limited telephony experience, and what little I know is from working on an old AT&T definity system, in a fairly passive role i.e. watching someone else do all the really cool stuff. Unfortunately, peons like myself weren't allowed to muck around on the PBX's. Lucky for me, Asterisk doesn't care who you are, you are encouraged to muck, and this book is an excellent place to begin.

Asterisk, The Future of Telephony, walks you through a brief history of telephony, and the technologies that have defined the industry since the dawn of time (more or less). The book is entirely self contained, and assumes very little previous telephony knowledge on the part of the reader. Aside from the AGI section, where some programming experience is taken for granted, anyone with basic linux admin skills should be able to read this book, and walk away with a strong Asterisk foundation to build upon.

The book is divided into 11 chapters which cover the full gamut of Asterisk's basic functionality. From preparing, installing, configuring and using an Asterisk system to writing customized scripts, connecting to VoIP gateways, and blocking telemarketers. If you need to do get something done, Asterisk is the PBX for you.

When working with open source tools, you get used to the phrase "that's really cool", and while working with Asterisk, that phrase comes up even more than usual. Every time I got through a chapter, I would have dozens of new ideas that I wanted to implement on my server and play with. I must have bored some of my colleagues to death with my "This is awesome..." speeches, but I don't think I was overstating matters at all. Asterisk really is awesome, and if the PBX big boys aren't worried, they probably should be.

If you are looking to setup a bunch of extensions in your house, leverage the power of VoIP, add capacity to your legacy PBX at the office, or do something entirely new, then this book is an excellent starting place and resource. My inner geek is very happy with the book, and I can easily recommend it to anyone w/even a passing interest in Asterisk.
