+++
title = "Chain Mail"
description = "Instrument of our doom"
draft = true
+++

Who would have thought, in 1996, that chain emails would be the instrument of
our assured mutual destruction? In 1996 I'd `telnet` into the university mail
server and type `pine` to check my emails. Invariably, there would be several
forwards from friends and family about how [Bill Gates and AOL]() would send
$1000.00 to everyone who passed this email on, or about the conspiracy theory du
jour.

[Netiquette]() dictated that you shouldn't send on these mass chain forwards, and
that you should confirm the validity of the claims you were sharing before you
sent them to everyone in your contact list. Unfortunately, most of the people
getting onto the internet in 1996 had no idea what netiquette was. I was the
annoying friend who'd reply-all to these messages with a nice "well, actually",
letting the sender and all recipients know that the claims in the emails weren't
actually true, usually with a link to the relevant snopes.com article debunking
the myth and that the sender should attempt to validate the claims made in their
emails before forwarding them on. I'm sure I was annoying, but I considered it
part of my responsibility to keep the internet a civil place.

Chain mails kept coming, and I kept "well, actually"ing them until around 2005.
Something happened in 2005 that I saw as a blessing at the time, the chain mails
started drying up. As facebook.com began allowing more and more users onto
its platform, more and more people started sharing what previously would have
been an email.  Perfect, I thought to myself, chain mails have moved to facebook
to die, and my inbox is now a calmer place.

But the chain mail didn't die, instead it thrived and evolved. Facebook had
created the ultimate chain mail echo chamber. Over the years, they added
algorithms and features that allowed you not to see posts from people you didn't
want to see, to bury comments from people who's opinions you didn't agree with,
and basically squash all the back-pressure that existed on a chain mail. The
"well actuallys" of the world were either silent, or buried in a sea of likes
and re-shares.

Hoaxes, conspiracy theories, get rich quick schemes, all had the perfect medium
to spread virally and infect millions of people with their half truths and
distorted messages. It didn't have to be like this. Facebook has the power to
apply back pressure to half truths and conspiracy theories. They could, like I
did, lookup claims on snopes.com and add a disclaimer, or a comment to posts
that weren't true "well, actually if you check this article on snopes.com, you
can see that no in fact Bill Gates isn't putting microhips in your vaccines" and
limit the spread of misinformation. But they chose not to.

In the rare cases they do add disclaimers, it's due to tremendous amounts of
customer/government pressure. Facebook lives and breaths engagement and eyeballs
and anything that takes away from those things is anathema to them. Of course
they wouldn't automate fact checking where possible, it might stop some post
from going viral which might cut into their bottom line. COVID-19 has killed 4
million people around the world so far and a huge chunk of those people have
been fed a steady diet of vaccine and COVID-19 misinformation by the facebooks
of the world. The chain mail has become a weapon that no one can control
spreading misinformation like a virus amongst the population.
