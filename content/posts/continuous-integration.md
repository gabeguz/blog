+++
description = ""
categories = [
  "tech",
]
date = "2014-01-31T15:00:00-05:00"
title = "Continuous Integration"
tags = [
  "ci",
  "jenkins",
  "development",
]

+++
I finally got around to setting up Jenkins for some continuous integration on
one of the projects I contribute to at work.   It's been a long time coming and
we've been talking about it for awhile.  Finally getting it done ended up
taking longer than expected.  No surprise there, with the constant pressure to
deliver and release, we often forget that quality and quality assurance are an
important aspect of shipping.  If you deliver something that's broken, then
you're not really delivering at all. 

The main thing we wanted to get from Jenkins was the ability to fire off our
unit tests in an integration environment on each git push.  We managed to do
this with a server side git hook, which curls Jenkins after each successful
push to the git repository.   What I didn't realize at the time was that
Jenkins can do so much more than just run your unit tests for you.  After
playing with it for the better part of a week, I now have a build script that
executes our unit tests, builds class docs, runs static analysis, lints the
code, and ensures that we are conforming to our teams coding standards.  

In the future I'd like to also see if we can't leverage Jenkins to push our
code out to staging + production after a successful build.  

There are a few things that surprised me about myself after my week with
Jenkins.  Perhaps the most surprising thing for me was how much a little dot
can influence my state of mind.  Red is bad, and green is good, and if you turn
my happy place red, I will hunt you down (just kidding, mostly).  

It's nice when the unit tests are passing, even though it obviously doesn't
mean that the system is bug free, or that our unit tests are perfect, or that
we have anywhere near the code coverage we'd like.   The static analysis gives
us our mess detection score, our levels of copy pasted code, and our CRAP
score.  What's more, we can track how those values change over time to see if
we're improving or not.

I know I've only just touched the tip of the iceberg with regards to what
Jenkins can do for me, if you have suggestions for further investigation, drop
me a line.  
