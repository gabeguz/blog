+++
categories = [
  "adventure",
]
tags = [
  "adventure",
  "skiing",
  "winter",
]
date = "2006-03-20T23:43:51-05:00"
title = "Cross Country Skiing"
description = "My first attempt at cross country skiing"

+++
So, I had my very first cross country skiing experience this weekend, and damn was it tough. I've been trying to get out and do some type of winter activity at least once a week since I've been living up here, and I've been more or less successful. Snowboarding, snowshoeing, shoveling snow off of the deck (does that count?), and now cross country.

The last time my friend Ben and his girlfriend were up here, they wanted to do some snowshoeing, so I hit up ol'faithful for some ideas, and came across Royal Gorge a huge cross country/snowshoeing only mountain pretty close to my parent's place in Truckee. I had no idea such a beast even existed.

We didn't go there that weekend, but I told myself cross country might be a fun thing to try one of these days. Well, one of these days ended up being yesterday, and overall it was a very good time. There are a few things that differentiate cross country from downhill skiing, the most noticeable being the price to get onto the mountain. Most big downhill resorts up in Tahoe (Northstar, Heavenly, Squaw) charge upwards of $60 for a lift ticket these days. Royal Gorge ran me about $50, including skis, boots, and a lesson.

Aside from almost passing out during the lesson (lack of food, water, and too much exertion), I had a great time figuring out how to get around on a pair of super skinny skis, and had one of hell of a workout in the process. I stuck to basics all day, just getting comfortable moving around, and doing the most basic "stride", or classical cross country technique. "Skating" is the more popular method of forward motion these days, and though I did try it a few times, I seem to expend way too much energy moving that way.

I was on the mountain for about 5 hours, 4 of them spent just wandering around on my skis, trying to maintain my balance, and work on getting my body to understand the necessary motions. By the end of the day, I was doing ok, and had thoroughly worn myself out.

A nice soak in the hot tub, and a beer later, I decided that cross country skiing is definitely more fun than running (i hate running) and something I'd like to get better at. Eventually, I imagine I could go out on some of the trails that up until now I've only done on snowshoes, and probably get to some pretty interesting places in the sierras. I'm sure my parents will be thrilled that I've found a more efficient way of getting lost in the mountains! 
