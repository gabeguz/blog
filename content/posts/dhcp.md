+++
title = "DHCP"
description = "Dynamic Host Configuration Protocol"
date = "2018-05-23"
tags = ["networking", "beginners", "development"]
cover_image =  "https://thepracticaldev.s3.amazonaws.com/i/4t2jhg0hna56u0xoccq9.jpg"
+++

Once upon a time, if you wanted to connect to the internet (or even to your local network) you needed to edit a bunch of files, ask your system or network administrator for an IP address, and hope you didn't mess up your computer too badly since you didn't know what you were doing.  Today, you just plug in your Ethernet cord, or login to your wifi network and you're good to go.  How does that work?

Assumptions
===========
- The network cable is good
- The port is active (plugged into a switch or hub at the other end)
- There is a DHCP server *somewhere* on the network

Preliminaries
=============
Ok, so you've got your cable plugged in, and you see some blinking lights, that's a good sign.  It means that the physical interface (the network card) is receiving information over the network, and possibly sending information over the network. 
You won't be able to reach hosts on the internet at this point as your computer doesn't have an IP address.  Your TCP/IP network information currently looks like this: 

```
|---------------|
|  My Computer  |
|---------------|
|IP: 0.0.0.0    |
|Router: 0.0.0.0|
|DNS: 0.0.0.0   |
|---------------|
```

No IP (Internet Protocol) address - The IP address is the address of your computer on the Internet.  It's how other hosts on the interenet know where to reach you.  When you browse to https://duckduckgo.com, you send your IP address as the "return address" so that the duckduckgo server knows where to send a reply.  

No Router address - A router is just a computer that is configured to transfer packets from one network to another.  All the machines on your local network can talk to each other without a router, as long as you know their IP address, you can send packets to them directly.  If you want to get to another network, you need to send your packets through a router that knows about both of the networks.  Usually a router is physically plugged into at least two networks via two ethernet cards, and can pass packets from one card to the other.

No DNS server address - We've talked about [DNS](https://dev.to/gabeguz/whats-dns-13c3) in a previous post, but basically DNS is your address book.  You ask for https://duckduckgo.com/ and the DNS server is in charge of looking that name (duckduckgo.com) up in its address book and telling you the IP address for that name is: 107.20.240.232 (promise, look it up).

No IP, now what?
================

What can you do w/out an IP address?   We can't reach other hosts on the internet yet, but we *can* talk to other hosts on our own network.  Not having an IP address means we can't tell others where to reach us, but we can YELL into the darkness and hope someone is listening. That's basically what your computer does when it's first plugged into a network (assuming you have a DHCP client running).  

```
you> HELLO?
you> HELLOOOOOOOOOOOOOOO!
dhcpserver> HEY!
dhcpserver> STOP YELLING.  Use the phone, call me here: 192.168.1.2
dhcpserver> And, here's a number you can use: 192.168.1.115
dhcpserver> Also, this is the number for the router, in case you need to get on another network: 192.168.1.1
dhcpserver> make sure you let me know you're going to use this.
you> oh, hey, thanks!  I'll remember that my number is 192.168.1.115 and yours is 192.168.1.2.  
dhcpserver> Great, oops, almost forgot, here is the DNS server you can use to lookup other computers: 192.168.1.3.  See you around!
```

Your computer (via it's dhclient program) sends a message called `DHCPDISCOVER` to the network broadcast address (a special address that anyone on the network can recieve messages to) this address is known by your computer ahead of time, and on TCP/IP networks is 255.255.255.255. Sending packets to this address will send them to all hosts on the local network.  Routers will *not* forward broadcast messages on to other networks.  If there's a DHCP server listening, it will reply with a `DHCPOFFER` message that contains all the information you need to configure yourself as part of the local TCP/IP network.  Since you don't yet have an IP address configured, the DHCP server will also use a broadcast message to send you the `DHCPOFFER`.  Then your computer will send back a `DHCPREQUEST` message asking the DHCP server to reserve the number it just told you about for you.  If everything goes well, the DHCP server will respond with a `DHCPACK` with any additional information (such as DNS server addresses) it has.  This message lets you know it's ok to start using that IP address officially on the network.

Now, assuming all that went well,  your network configuration should look like this: 

```
|--------------------|
|     My Computer    |
|--------------------|
|IP: 192.168.1.115   |
|Router: 192.168.1.1 |
|DNS: 192.168.1.3    |
|--------------------|
```

You can confirm your current ip address using the `ifconfig` command on most unix like systems: 

```
$ ifconfig iwm0
iwm0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
        lladdr e8:2a:ea:ab:49:24
        index 1 priority 4 llprio 3
        groups: wlan egress
        media: IEEE802.11 autoselect (HT-MCS14 mode 11n)
        status: active
        ieee80211: nwid Odessa chan 11 bssid ac:f1:df:62:d6:9a 70% wpakey wpaprotos wpa2 wpaakms psk wpaciphers ccmp wpagroupcipher ccmp
        inet 192.168.0.115 netmask 0xffffff00 broadcast 192.168.1.255
```

You can confirm your router address using the `route` command: 

```
$ route show
Routing tables

Internet:
Destination        Gateway            Flags   Refs      Use   Mtu  Prio Iface
default            192.168.1.1        UGS        9      860     -    12 iwm0 
```

And you can confirm your DNS server by looking at the contents of /etc/resolv.conf

```
$ cat /etc/resolv.conf
nameserver 192.168.1.3
lookup file bind
```

Questions
=========
Congratulations, you're on the internet!  A few questions to think about: 

1. What would happen if you decided to run a DHCP server on your laptop and start passing out IP addresses on your network? 

2. What more can you do before you have an IP address? 

3. What allows you to have connections to other computers on the network before you have your TCP/IP networking configured? 


