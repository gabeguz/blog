+++
title = "$DREAM_JOB"
description = "Thoughts on the perfect job"
date = "2017-12-05"
tags =  [ "culture", "career", "leadership" ]
+++

Everyone has some idea of what their ideal job would look like.  Unlimited vacation, respect, a company car, power over your destiny, meaningful work, etc. Most of us probably don't currently work at our dream job and so we hop around looking for a better deal.  I try to mold my current job into the job I want it to be.  

What are some examples of what I mean?  Several companies ago I was working somewhere that didn't have an automated deploy process, and regularly ssh'd to production to make changes.  In my mind, the ideal workplace would have some kind of automated process to get changes into production, so I set one up.  No one asked me to do it, I just felt it was important and did it.  

Another example, from a different employer, when I arrived there was no code review practice in place.  I talked to the team and suggested we should try requiring at least one other person's approval before allowing a change set into the master branch.  There was some arguing at first, but we decided to try it for a sprint.   In the end we decided to keep doing it and I felt like I'd made a change for the good.

On a non technical note, the first team lead I ever had said something to me that I've tried to carry over everywhere I've worked:  "I always have time for my team, even if I look super busy, please interrupt me."  This is an attitude change that costs almost nothing to implement and has an outsized impact on your team.  Just letting them know that you're there for them helps set a positive tone.

What are you looking for? Is it more autonomy?  Is it more impact?  Is it a company car?  If you can't list the things that you're looking for, you won't be able to make changes towards those things.

How do you get there?  Ask yourself "What can I do right now to move $CURRENT_JOB towards $DREAM_JOB?"  And then, look at the outcome of that.  Did your action have any impact?  

If yes, congratulations, you are now one small step closer to having your $DREAM_JOB.  Lather, rinse and repeat. 

If not, all is not lost, but this could be a sign that your current employer is not open to change.  Try again, and see what happens.  I take the view that we can all be agents of change regardless of where we sit on the org chart.  There are things I can do on a daily basis that will impact those around me.  Some simple examples:

- being helpful 
- being positive
- empathizing with others
- implementing process changes
- automating tedious tasks

This may not work in all situations.  You need to gauge the impact your changes are having.  Are people responding?  Is the climate such that your changes can work for the good?   Sometimes you come to a roadblock.  Sometimes you get a clear message that what you're trying to do is not supported by your employer.   Eventually, you have to ask yourself a harder question:  "Is it worth pushing for change here, or should I try to find somewhere that's more open to change?"  In my opinion, as long as your changes are working and you're happy with your compensation, then keep pushing to make things better.

And of course, if your $DREAM_JOB *does* come along, well... take it.
