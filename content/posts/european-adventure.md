+++
categories = [
  "adventure",
]
tags = [
  "travel",
  "europe",
  "luxembourg",
  "belgium",
  "france",
  "switzerland",
  "netherlands",
  "austria", 
  "germany",
  "england",
  "italy",
]
description = "A log of my trip to Europe"
date = "2002-06-29T22:50:25-05:00"
title = "My European Adventure"

+++

Well, I have decided to leave Student Computing after about 5 years of fun filled work. The decision was not lightly made and actually caused me quite a bit of anguish. Luckily, I have managed to save up a fair amount of money while working at USD and so can now take some much needed time off. That's why I'm going to Europe for two months to see the things I saw when I was two but, to my parent's chagrin, no longer remember. Hopefully I will have a great time, and use this page to put up interesting info about where I am and what I am doing. If I have time, I may even post a few pictures. Don't expect anything, and you may be pleasantly surprised!

6/29/02 Back in S.D.! Had a great time in Europe. I will update the rest of my travels later this week. For now, here are the last of the pictures:
Luxembourg, Luxembourg
Brussels, Belgium
Antwerp, Belgium
Paris, France

6/21/02 I'm in Antwerp still, having a pretty good time. I found a cheap internet cafe and so decided to post the rest of the pictures I had gotten put onto CD. I won't put the rest (Luxembourg, Belgium, Paris) up until I get back to S.D., but here are these for now:
Salzburg, Austria
Vienna, Austria
Berlin, Germany
Hamburg, Germany

6/19/02 Today. Today I was on a mission to find the statue of the little boy peeing, "Mannequin Pis." Once I found him, I explored more of the city and made my way to this internet cafe. From here I am going to find dinner, another waffle, and my bed. Tomorrow morning I am off to Antwerp for a few days, followed by Paris for a few more, and then back to London for my flight home. I dont know if I will update the page again until I get back to San Diego, but I will do what I can.

6/18/02 Belgian Waffles. Mmmmm.
Got to Brussels today and after dropping the pack off at the hostel, I went for a nice self guided tour of the city. I had dinner off of the Grand Place along this street that is literally suffocating with restaurants, the host of each standing outside offering a free drink to any patron choosing his establishment over the next one. Not only that, but doing so in English, Spanish, French, and German.

6/17/02 Today I asked the hotel owner where I could find good regional food. He gave me the name of a restaurant and directions which I followed. Once there I looked over the menu and decided I felt like spaghetti. So much for finding regional food. Sometimes I guess you just have to have spaghetti. At least it was really good.

6/16/02 Got in to Luxembourg late last night and found a place to stay. Spent most of today walking around the city doing the tourist thing. Luxembourg is a huge change from Amsterdam. I spent some time in these really cool underground fortifications that are beneath the city and fell down a slippery spiral staircase which hurt quite a bit but was apparently very funny to watch as the lady behind me couldn't manage to contain her laughter. Oh well, I probably would have been laughing too if it hadn't hurt so much.

6/15/02 - 6/16/02 Monica, her friend Aaron, and I drove up to Amsterdam sort of spur of the moment this morning. Amsterdam isn't much to write home about. The streets were covered with trash, every third person would walk up to us and ask "cocaine, extasy?", and the whole place smelled to either urine or pot or both. The Anne Frank house and the pubs open until 4am were two redeeming factors, but overall I don't think I would have died if I didn't get to see Amsterdam this trip.

6/13/02 - 6/14/02 Spent these two days at my friend Monica's house in Hanau, Germany. Didn't do much excpet walk around town and sleep in.

More Pictures!
Interlaken and it's surroundings, Switzerland
Bern, Switzerland
Stuttgart, Germany
Heidelburg, Germany
Nurnburg, Germany
Munich, Germany
Dachau, Germany
Garmish, Germany
Fussen, Germany

6/11/02 - 6/12/02 Spent two days in Hamburg, exploring the city

6/10/02 I spent all of today wandering around Berlin. It is another amazing city. The remains of the wall as well as the 'comrade' walk lights in the Eastern part of the city were especially interesting. Unfortunately, the Bradenburg gate was undergoing some serious work and so was totally covered. It was still neat to see.

6/09/02 Vienna is awesome! Except for their internet cafe, which I am in right now. They have Windows XP on their machines, which is good since I can just plug my camera in and get the pics off of it without having to reboot. But, ftp doesnt work, I cant email the files to myself, the web uploader on students doesnt work, and smb doesnt work. Add to that that I cant get a command line window to come up and Gabe becomes a very annoyed customer. So, I have been sitting here for an hour and a half trying to upload some new pictures and have been completely unable to do so. Can we say FRUSTRATED. About the only good thing about this giant waste of time is that I now only have to wait about 30 minutes for my train to leave (that, and Dillon was online so I got to chat with him and hear all about the fun he is having at work). Ok, I will write more about Vienna when I get to Berlin, my next stop.

6/08/02 Brought an early morning train to Salzburg and spent all day walking around and exploring the city. It is a pretty nice place. As a bonus, the University was having a 'Music Day' so there were hundreds of students either playing instruments or singing all around in the various plazas and Uni. buildings. Some of my favorites were listening to this guy playing on one of the huge church organs in the main Cathedral, and listening to two bands attempt to cover American rock and punk. They actually did a pretty good job, especially when the rock group closed with Deep Purple's "Smoke on the Water".

6/07/02 Ok, here's the story I promised:

Two days ago I went to Füssen to visit the two castles it is famous for. On the return I was going through all the pictures I had taken on the digital since I left Interlaken and writing down what they were in my journal for later transcription to the webpage. I also had my Eurail pass out to show the conductor when he came by. For whatever reason (I blame fatigue) I didn't put the pass or the journal into my backpack when I was finished with them, I just left them on the little table next to my seat. Once the train got back the the München Hbf (Munich, Main train station) I grabbed my backpack and my fleece and left the train. I grabbed the underground back to my hostel and put my bag down on my bed while I started to load things into my big pack as I would be leaving in the morning. After a while, I reached for my journal to get the calendar out of it so I could write down where I was that day. I looked around for a few minutes and didn't see it. Hmm, I checked my backpack, nope not there. Crap. Where is it? I went through my large pack since I had just been stuffing things in there maybe I accidentally dropped my journal in as well. Nope. "Oh well, it´s not that big of a loss" I thought to myself, and then "OH CRAP, MY EURAIL". At this time I frantically shove all of my things into the locker I had at the hostel, lock it and run back to the underground looking around for possible places I could have dropped it. Nowhere, back to the train station, into the shop I bought some peanut butter in after getting off the train, not there. Up to the lost and found "No, sorry no journal's or Eurail passes have been found today, try back tomorrow around 11 or so." AAAAARRRRRGGGGG.... "I'm an idiot, a moron, of course, how could I be so stupid." THWACK, THWACK, THWACK (me slapping my head with the palm of my hand). ARRRRRRGGG. Ok, relax. Back to the hostel. Sleep (actually more like: try to sleep, toss and turn, try to sleep some more, eventually sleep).

Next day: Sleep in, take a shower, I am calm now. I have resigned myself to the fact that I will never see that pass again. I eat breakfast at the hostel, do some laundry that badly needs doing, sit on the bed, look at the watch, 9:45, too early. I finish my laundry, some of it is still damp so I hang it on the bed. Look at watch, 11am, lets go to the train station again. "No, I'm sorry but we still haven't recieved a journal or a Eurail pass. Here is the number for the central lost and found office, you should call them." I go to a phone "No sir, we have no report of a found journal or Eurail pass, please leave your name and we will open a case number for you." I leave my name, and become case number 9259177. I am told to try call back in a few days to see if anyone has turned it in. Fine, ok, I can do that, I already extended my stay at the hostel just in case. What now? Walk around Munich, eat lunch, walk some more, buy a book (really 2 books), walk around, read the book, eat dinner, read the book (with dinner), go to hostel, finish book, go to bed.

I wake to the sound of rain pouring down outside the window, that´s nice, I like rain. Breakfast, check out of the hostel, and back to the train station to drop my pack off in one of their 24 hour lockers. I need to call the lost and found main office tomorrow, today (6/7) I need to figure out what I am going to do with the rest of my trip. I can change my flight to an earlier one. Without that Eurail I am pretty screwed. I bought the insurance so I can pay my own way for the rest of the trip and they will probably reimburse me when I get back. I could cut my trip short a week and cut out a few places I had wanted to visit, that way I have enough money to pay for the train tickets I still need to buy. I could go check how much a flight costs to London and fly there when I am done since it is cheaper than the train. Ok, I need to go look up a bunch of things on the Internet. Get to the Internet cafe, check my email. That's interesting, message from mom, subject: Eurail. I read it "Gabe, got a call from a German gentleman, he says he has your Eurail Pass. Is everything ok? Love Mom." Is everything ok? It is now. Logout, run to train station, dial number. "Hello, yes, I have your Pass. I have been trying to contact you since the 5th. Really? That is odd, as soon as I found it I reported it to the lost and found office in Munich and gave them your name as well as a description of what was lost. You don't say, the next day around 7am I also called the central lost and found office and reported the same thing to them as well as giving them my phone number. They really said they had found nothing? They sound like social servants, well I have it but I am very busy today. Perhaps we can meet at the Train station when I finish work?" Fine, great, grand, wonderful, thank you God. Call central lost and found, "Hello, yes, I speak a little English. Your number please. 9259177? Hmm, no I have no record with that number are you sure it is correct? Perhaps call back later. Goodbye." Ok, situation normal.

6/06/02 I was just reading slashdot.org and came across this story on credit card fraud and terrorists. It is pretty interesting, so if you are bored, check it out.

6/06/02 AAAARRRRRGGGGGG! I did something really stupid yesterday. I'll fill everyone in once the situation resolves itself.

6/05/02 Went to Füssen today to visit Neuschwanstein and Hohenschwangau castles. They were amazing, I got some pretty good pictures of them that I will put up here eventually. For now you will just have to imagine it.

6/04/02 Dachau. The very first concentration camp. I'm not going to say anything about it. You should go visit it to see for yourselves.

6/03/02 In Munich spent the day... yes, you guessed it, walking around the old part of the city. Some pretty nice churches here in Munich and fountains as well. Went to the world famous Hofbrau house and had a giant beer. I can´t believe I actually finished the whole thing, the glass was bigger than my head and had a liter of the golden liquid in it. To my credit (or discredit depending on who you ask) it did take me 3 hours to finish. Note for Elizabeth: On the way home I saw a string quartet playing for money in the street. They played some Vivaldi, Bach, and... wait for it... Mozart. You would have cried, it was beautiful, they were really good.

6/02/02 I'm in Nuremburg! Going to Munich tomorrow, more updates in the next few days!

6/01/02 Today was busy. Got up early and went to the Germanisches National Museum. It is sort of a German history museum that takes you through from the stone age to modern times. Musical instruments, weapons, stained glass windows, cookware, tools etc. All in all a nice museum, but really big. After that I headed over to Luitpoldhain a sports complex Hitler had planned and was in the process of building until WWII sort of put a permanent halt to his plans. The parts that were finished are impressive to say the least. There is the Great Road, which is about a half a mile long and probably 60 yards wide. It is the road that the SA would march down during Nazi political rallies. The Zeppelin landing field, where Hitler gave most of his speeches is this giant field that all the soldiers would march into from the Great Road and stand there while Hitler went off on some speech. And then there is the almost completed Congress hall which is where the museum now is. It was a really well done exhibit. It takes you through the Nazi rise and fall from beginning to end. That pretty much took me all day. I even forgot to eat, it was that good.

5/31/02 In Nuremburg: Nothing is closed! Fabulous, and there are no holidays planned for awhile I think. And, there is a Bierfest going on practically outside my window. Ate the local Nurmburg specialty Bratwurste, small sausages similar to American breakfast sausage and walked around the old part of the city.

5/31/02 Today I went to the Porsche and Mercedes museums. The Mercedes one is definitely the better museum. They take you through the whole history of cars basically and have a great display of tons of cars. The Porsche museum was a little dissapointing, but still cool. Off to Nuremburg right now.

5/30/02 Went on a day trip to Heidelberg today. It's a smaller city with a pretty cool palace ruin. Spent the day walking around the city and exploring the palace.

5/29/02 Made it to Stuttgart today and spent most of the day walking around this giant park that is right in the middle of the city. Apparently both Mercedes and Porsche have museums in this town. Also made it to a biergarten towards the end of my walk to enjoy a nice half liter of Hefewizen. Very tasty. I can't forget to mention that the bad luck with things being closed must just have been me since tomorrow is apparently a national holiday and, yup everything is going to be closed.

5/29/02 I'm leaving my good friends the Pfeifer's this morning and am heading to Germany. I'm not sure when I'll get online again so you may have to wait awhile for more updates/pictures. We'll see. I'm happy to say that I have finally found the apostrophe on these keyboards though.

5/28/02 Today we did some yardwork. I'd say it was a pretty good deal for me: 4 nights of room and board with planned excursions and plenty of laughs for a little more than 4 hours of clipping hedges, and mowing the lawn. I wish I could get that kind of deal for rent in San Diego :)

5/25/02 Ok, hopefully you all got to see the great picture's I put up earilier this week and hopefully you all thought I was crazy for not having any pictures that really show anything of what I've done. If not, well that's just too bad, my joke went to waste. In either case, here follows a list of links to several pictures I've taken on the digital since I left SD. I tried to edit a bit for content so as not to overwhelm you since I have been rather camera happy since I got here, I don't have to pay for film or developing on the digital after all. Even with my editing, there are still a ton of pictures, so take it slow and you won't hurt my feelings if you don't look at all of them.
Trip, Sky and various airports
London, England
Oxford, England
Stratford upon Avon, England
Stonehenge, England
Salisbury, England
Nice, France
St. Veran, France
Como, Italy
Lucerne, Switzerland

5/25/02 Today the Pfeifer's took me to Bern, Switzerland's capital. We walked around the old city, saw the Cathedral, and the clock towers. Got to see several of the fountains that line the main street as well as the Bears that Bern is named after. We ate lunch at Migros, what they call a "self serve" restaurant here in Europe. We'd call it a cafeteria. It doesn't matter what you call it, the food was good. They had a party to go to tonight so I'm on my own and that's why I have some time to update the webpage.

5/24/02 Sad day today. Had to wake up at 5am again and say bye to Katrina. She flies home at 10:30 and I'm off to Interlaken at 7. I got to Interlaken at 10am and met the Pfeifer's for the second time. I met them the first time when I was 2 and in Switzerland. They drove me around Interlaken and the surrounding area today and I got to get a feel for where they live. That brings us up to date for now. I will post more as I have the time/resources.

5/23/02 Spent all day wandering through Zurich. It's the biggest city we've been to in Switzerland and yet it is still really small. Most of the interesting things to see line either side of the river and you can easily walk through all of the older parts of the city. There are three Cathedrals all within a 300 yard radius and you are allowed to climb the tower of Grossmunster Cathedral. Dinner at the "Crazy Cow" a fun restaurant.

5/22/02 In Zurich now.

5/21/02 What do you know, everything is open today! We walked around the shops of Lucerne, I bought a few gifts for some people... well I might just keep them for myself so maybe I didn't buy any gifts, only time will tell. Katrina bought some "bowling shoes" today. They are pretty popular over here and the one's she got don't look too much like bowling shoes so... well, I'm still going to give her a hard time about it, and all of you should too! Just don't tell her I told you to. I almost left out the best part of our day. While we were eating dinner at this small Swiss fondue place we saw a bunch of people carrying alp horns (you know the ones... RICOLA) walking down the street. After we finished our meal we walked in the direction they were heading and came across them eating a small meal in a riverside cafe. We hung around on the off chance that they might start playing after their meal. When they were done eating they got up and walked a little ways up to this open square and all 11 of them (10 guys and a girl) set up their horns and played for about 15 minutes. It was incredible. Those things sound amazing when you are that close to them. The music was great, and the musicians played with passion. When I told a Swiss friend of mine that there were 11 people in the group they were quite surprised and told me that the alp hornists usually only travel in groups of 4 to 5. Lucky us!

5/20/02 Oh, I almost forgot. In keeping with the tradition of things being closed when Katrina and I arrive somewhere, most of Lucerne's shops are closed on Sunday as well as on the 20th of May for a holiday. Luckily, the big tourist attraction, Mt. Pilatus, was still open so we headed for the boat station and bought some tickets. We took a boat to the base of the mountain and then got on Switzerland's steepest Cog Wheel Railroad which has inclines of up to 48 degrees in some places. Once up at the top we hiked to the summit for some fairly decent views through the clouds. Lunch on the top consisted of Pork Bratwurst with mustard and bread, it was great! On the way back down the mountain, by cable car, we stopped half way and took a ride on Switzerland's longest tobogan run. That was really cool. I had my GPS with me and it clocked me going 26.3 mph. I'm sure Katrina was probably going faster than me though so I should have given her the GPS. After Pilatus, (oh, the summit is at about 7000 ft) we walked around Lucerne some more and then called it a day.

5/19/02 Today we went to Lucerne's glacier garden and walked around. We saw the Lion Monument, the glacier garden, a neat hall of mirrors, and an old Swiss house set up as a museum. The glacier garden didn't actually have any glaciers, only the signs of their having been there in the past. There were also signs that Lucerne's environment had once been tropical in nature. The hall of mirrors was really cool too but most of the pictures I took on the digital while in there didn't come out. I may post one or two of the better ones.

5/18/02 Today we arrived in Lucerne. Switzerland is beautiful. So far it's my favorite place. Everything is a lush green and the mountains are amazing. The train brought us up the middle of Switzerland where we were treated to waterfalls and mountains at every turn.

5/17/02 Yet another 5am wake up. Kat and I travelled to Lake Como in Italy today and spent all day exploring the town and lake area. Found a pretty good pizza place and grabbed some dinner there. Woke up early the next day and walked around a bit more, grabbed lunch at the same pizza place and headed for the train station. Como was really hot and I had a bit of hay fever which is odd since I don't usually have that problem.

5/13/02 - 5/16/02 Woke up early again this morning. Had a 10am train to Mt. Dauphin Guillestre up near the French Alps that form her border with Italy. From there we took a taxi up to this tiny town in the French Alps called St. Veran. The first day we were there, everything was ferme (closed) and didn't open until the 15th. Luckily on the 14th the local bread store and a small alimentation (food store) were open so we were able to buy some baguettes, sausage, potato chips, and tuna fish to hold us over another day. We went on a 6 mile hike up into the mountains on the 14th and made it to within about a quarter mile of the French Italian border. The next day, the 15th we hiked the 4 miles to a slightly larger town Ville Vieille. They actually had an ATM and a bar that was open. We took a taxi back up the Mountain to St. Veran and had dinner at the one open restaurant (it had just opened that day). On the 16th we took things a little easier and just walked around St. Veran. The second restaurant in the town opened on the 16th so we got dinner for the second night in a row... talk about luxury!

5/12/02 Woke up too early today. Had to catch the 6am flight to Nice. Got to Nice around 9am and spent the whole day walking around the city. Nice is a very nice place, I liked it alot. We saw the beach, the ruins of an old Chateau, and plenty of little shops with all kinds of wares. We had some great crepes in Nice and were really tired by the end of the day.

5/11/02 This morning was my last day in Oxford. Today Kat and I headed into London once again to check out the market on Portobello Road. It was pretty neat. All kinds of antiques and trinkets. You could easily fill a house with junk in no time at all shopping there. After that we had tickets to a performance of Shakespeare's "12th Night" at the Globe Theater. It was AWESOME. The players were all men just like in Will's day and they worked incredibly well together especially for opening night. We also had great seats and I strongly recommend you try and sit in them if you ever happen to go. Gentelman's Room 9, any seat.

5/10/02 A day of rest. Slept in today and just took it easy. I met up with Katrina for lunch and then we headed into the city centre and up to the top of the oldest building in Oxford, the Saxon Tower. Later that night we headed out to the Turf, a cool little pub down a rather dark alley.
5/25/02 I'm in Interlaken Switzerland and finally have some free time so I can update everyone on what I've been up to over the last few weeks. The next several updates will be between lines so that it's easy to distinguish them from the rest of the material that is actually in cronological order.

5/22/02: Im still having apostrophe problems and Im running out of time on this computer, but I finally got some pictures saved and am uploading them right now. Click Here to see.

5/18/02: Im in Lucerne and cant find the apostrophe on this keyboard. More tomorrow.

5/09/02: Katrina's taking a test right now, but the plan is to head to Stonehenge when she's done. This is probably the last entry for awhile since, to paraphrase Wayne's World, "St. Veran (next town I'm going to) doesn't even have A computer, much less several that would necessitate an Internet Cafe."

5/08/02: Today was Shakespeare day, we went to Stratford upon Avon. Got to check out Will's birthplace and his house, as well as Anne Hathaways house. Saw the 3 theaters and walked along the Avon for awhile. When we got back to Oxford I went out with Katrina and her friends to a bar/club called Que Pasa. Latin music in Oxford? Who knew?

5/07/02: Blenheim Palace. It was cool. After that, back to Oxford for a beer at the Eagle and Child, the pub Tolkien and CS Lewis are said to have frequented. I really need to upload some pictures. Also went to The Turf, another pub that's really hard to find. Luckily I have a very good guide.

5/06/02: Got in to Oxford last night around 8pm and passed out. I walked around Oxford today while Katrina was in class, Visited some of the College buildings as well as St. Andrews. After Katrina was done with class, we went down to the city centre (who spells center like that?) and walked around and went punting down the river for an hour. Punting is... interesting. It's really rather hard to do. Today was much more relaxing than yesterday and I don't seem to be jet-lagged. Oxford is a very nice place and I'm having great fun here.

5/05/02: Hung out in London all day today. Went to Buckingham, 10 Downing St, Cabinet War Rooms, Imperial War Museum, London Eye, Big Ben, Parliment, Tower Bridge, London Bridge, saw the changing of the guard, visited the British Museum, and got really really tired from staying up all day. I have a fair number of pictures that I need to post, I just need to find the time/place to do it.

5/05/02: Well, I made it. More to come!

5/04/02: 2:00am Only 6 hours until I get on the plane, and I'm almost ready to go. I can't wait! I'm turning my computer off now. See you in Europe.

4/30/02: Only 4 days until the trip starts. I'm excited, just thought I'd share.

4/18/02: The fabulous people I work with took me out to dinner tonight to Karl Strauss, it was very tasty! Thanks for the card, and the gift, and the great times we've had over the last few years.
16 days until the trip starts!

4/12/02: Just wrote a perl script to auto thumbnail and link any pictures I put up here. Here's the code and Here is a test of that code. Happy Birthday Daniel!

4/8/02 : Ok, the trip begins on May 4th with my flight from SD to Heathrow Airport in London. I'm pretty much ready to go now though, my bags are packed and my bills are paid. Happy Birthday Dad!
