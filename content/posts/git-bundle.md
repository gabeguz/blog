+++
title = "git bundle"
description = "TIL"
date = "2018-05-30"
tags = ["til", "git", "development"]
+++

I had an interesting problem at work the other day.  We'd hired some external contractors to build an android app for us, and now we needed to get the code from them and into our private GitHub.  The problem was, I couldn't quickly get them access to the repo since there's a fair amount of process involved in getting those approvals.  They also didn't have enough seats to add me as a contributor to their GitHub account, and so we were stuck with me asking them to send a .zip file with the code.   Instead, they came back to me with a git bundle file, which I'd never heard of (though I've been using git for a long time now).   A git bundle file is essentially a full repository in a single file.  You can have branches, history, tags, basically everything you expect in a repository, but it's all contained in a single file.   This makes sharing the repository or moving the full repository pretty straightforward.  Assume I have a git project called myproj, and I want to send that to someone else.  


```
host1$ cd myproj
host1$ git bundle create repo.bundle master
```
That will make a bundle file (repo.bundle) that contains all the history of the master branch.  You can then send this file to a friend, and they can clone it just like if it were a remote git origin: 

```
host2$ git clone -b master ./repo.bundle myproj
host2$ cd myproj
host2$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```

Another pretty neat thing about git bundles is, if you need to update one, you can send an incremental file update with only the latest changes.  The recipient just puts that in place of their existing bundle, and runs `git pull` w/in their repo.  

This isn't something you'd use every day, but in certain situations (like getting a git repo from one place to another w/out being able to share a remote origin) it works great!

More info on git bundle: https://git-scm.com/docs/git-bundle
