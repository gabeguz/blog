+++
title = "Goodbye Google"
date = "2020-10-23"
description = "Searching for and switching to alternatives to google services"
categories = [
  "tech",
]
tags = [
  "google",
  "tech",
  "selfhosting",
  "opensource",
  "OpenBSD",
]

+++

I've been a gmail user since it first came out, and I used to be a fan of google, but now I find myself more and more concerned with their data gathering practices.  I've decided to move all of my personal services off of google onto open source self hosted alternatives. This page will act as a checklist for each of the services I'm able to get off of. Each of the links below will talk about what I'm using now and how I set it up.


## Google Services To Replace

- [x] [search](#search)
- [x] [browser](#browser)
- [x] [email](#email)
- [x] [calendar](#calendar)
- [ ] [photos](#photos)
- [ ] [contacts](#contacts)
- [ ] [maps](#maps)
- [ ] [android](#android)
  - [x] [authenticator](#authenticator)
  - [x] [play store](#play-store)

### Search

This one was the easiest of the bunch.  Instead of using google.com as a search
engine, I now use duckduckgo.com.  I've made duckduckgo.com my default search
engine for *everything.* There's even a command line tool that lets you search
from the cli: https://github.com/jarun/ddgr

### Browser

Instead of using chrome, I'm now exclusively using
[Firefox](https://getfirefox.com).

### Email

Email was complicated since there are so many moving parts.  Instead of gmail,
I'm now using a combination of [OpenBSD's smtpd](https://man.openbsd.org/smtpd),
the [dovecot imap server](https://www.dovecot.org/), and [OpenBSD's
spamd](https://man.openbsd.org/spamd) hosted on a [Vultr
VPS](https://www.vultr.com/) running [OpenBSD](https://www.openbsd.org).  To
read and send mail I'm now using [aerc](https://git.sr.ht/~sircmpwn/aerc) as
well as [Thunderbird](https://www.thunderbird.net/en-US/) as my local clients. I
don't do webmail anymore.

### Calendar

For my calendar, I'm now using [simple
calendar](https://www.simplemobiletools.com/calendar/) synching to a self hosted
instance of [kcaldav](https://kristaps.bsd.lv/kcaldav/). I pretty much only use
my calendar on my phone so I don't currently have a web version.

### Photos

No good solution for photos yet.

### Contacts

Haven't tried to figure out contact syncing yet.

### Maps

While I've used [openstreetmap](https://openstreetmap.org) it's not very
effective where I live.  I've been contributing to it when I can, but it can't
yet replace google maps for me.

I recently found [Magic Earth]() which, while not open source, doesn't track
you.

### Android

I know there are some open source forks of Android out there and I've tried
[CyanogenMod](https://en.wikipedia.org/wiki/CyanogenMod) previously (which I
guess is now called [LineageOS](https://lineageos.org/)), but I think ideally
I'd like something like the [PinePhone](https://www.pine64.org/) to fully opt
out of the Android ecosystem. 

#### Authenticator

Instead of Google Authenticator I'm using
[andOTP](https://f-droid.org/en/packages/org.shadowice.flocke.andotp/). 

#### Play Store

Instead of the play store, I'm using [F-Droid](https://f-droid.org/en/) a
catalog of open source software for Android.

