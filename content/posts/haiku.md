+++
categories = [
  "random",
]
tags = [
  "haiku",
  "poetry",
]
date = "2005-11-23T23:28:21-05:00"
title = "haiku"
description = "Some Haiku from my old blog"

+++
19 April 2006
=============
## Peeps

Little yellow Peeps,

Sugar covered Easter fare

Marshmallow goodness

11 April 2006
=============
## Workplace Haiku

a new day begins

the ringing phones my soundtrack

welcome back to work

7 April 2006
============
## Workplace Fib
(http://gottabook.blogspot.ca/2006/04/fib.html)

I

am

not so

wonderful

as a manager.

my skills need some development.

i was once a geek,

that was cake

compared

to

this

2 March 2006
============
## Workplace Haiku

sitting in my chair

my lower back begins to ache

i will go to lunch

8 March 2006
============
## Workplace Haiku

In the conference room

Powerpoint on the big screen

Trying not to sleep

13 March 2006
=============
## Webmaster Haiku

My photo pages

They all seem to be broken

I wonder what's wrong


## Workplace Haiku

by myself at work

all my people called in sick

what, oh what to do 

28 February 2006
================

## Workplace Haiku

In my cubicle

Corporate life not for me

Time to retire 

23 November 2005
================

## Bee Haiku

always remember

death can come at any time

even from a bee 
