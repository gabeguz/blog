+++
title = "I am an open source developer"
date = "2020-10-27"
tags =  [ "opensource", "software", "philosophy" ]
+++

I'm an open source developer.  What does that mean to me? 

## Prefer open source software

I always prefer open source software. If no open source software exists to do what I'm trying to do, see the next point.

## If the software doesn't exist, create it

If there's no software available to do what I'm trying to do, it's my job to create it.

## Contribute to open source software

If I use open source software and find a way to improve that software, or fix a bug, it's my job to improve it. 

These statements are aspirational, not absolute. There are always exceptions and I'm not going to spend my life worrying about what piece of not free software I used today.
