+++
title = "I Can't Feel My Legs"
draft = "true"
toc = "true"
+++

# I Can't Feel My Legs

A Story of Health Care in Quebec

## Outline

- Introduction
- Timeline of events
  - Early Symptoms
  - First Visit to Optometrist
  - Visit to Ophthalmologist
  - CT Scan
  - Neurologist
  - MRI
  - ER
  - Admission to Hospital
  - Diagnosis + Treatment
  - Release from Hospital
  - Recovery
  - Return Home
- Final thoughts
  - Should have gone to the ER sooner
  - Cost breakdown
  - Healthcare in Quebec

## Introduction

Back in January of 2018 I wound up in the hospital for 3 weeks due to a rare
autoimmune disorder called NMOSD (Neuromylytis Optica Spectrum Disorder) that
affects about 4% of the population. This is the story of my initial symptoms,
eventual diagnosis and ongoing treatment.

## Timeline of Events

### Early Symptoms

In October of 2017 while taking a shower, I noticed a [sunburn like
feeling](https://en.wikipedia.org/wiki/Paresthesia) on my chest. There weren't
any visible signs of irritation, but even the mist from the shower caused me
some discomfort. I assumed at first that maybe my 6 month old daughter had
kicked me while we slept, and I had some bruising from that. I didn't think much
of this pain, and just assumed it would go away. The pain was minor enough that
I didn't really notice it most of the time and it soon faded off into the
background of daily life.

One night, while binge watching a show that I've now completely forgotten, I
felt some pain towards the back of my eyes. It felt quite a bit like eye strain,
and I assumed I'd just stayed up too late watching videos on my phone. The pain
persisted into the next day as I walked to work. I told myself I'd keep an eye
on it, and that if it didn't get better by the end of the week, I'd go see an
optometrist. I needed to renew my eyeglasses prescription in any case.

As the week progressed, not only did the eye strain feeling not go away, but I
started to notice other symptoms with my vision. The two major ones were that
all lights appeared dimmer than they actually were, it was like someone had
gone into the settings and turned down the brightness levels for my life. Lights
at work, at home, even sunshine seemed to be turned way down. I also started
having strange blurriness in my vision. When reading text, one word would be
clear, the next would be blurry, and the following word would be clear again.
This pattern repeated throughout the whole text. Interestingly, each eye had a
slightly different pattern, so I could still read by alternating which eye was
open as I read. Definitely not ideal. I immediately made an appointment with
they optometrist and went to get my eyes checked.

### Visit to the Optometrist

The optometrist tested my vision, gave me a new prescription for glasses and
then did a thorough examination of my eyes. Nothing seemed out of the ordinary
but based on my symptoms, she decided to refer me to an ophthalmologist just to
be safe.

## Visit to the Ophthalmologist (Nov 14th)

I was referred to the Ophthalmoloty department at the [Hopital Notre
Dame](https://ciusss-centresudmtl.gouv.qc.ca/etablissement/hopital-notre-dame)
in Montreal. When I went in for my appointment, they ran several eye tests, and
the Dr. examined my eyes with an 

the Ophthalmologist told me they'd found some [swelling of my optic
nerve](https://en.wikipedia.org/wiki/Papilledema). She also told me that based
on the results of my eye tests, that my visual field and colour vision were
impaired.  She informed me that swelling in the optic nerve isn't something that
I could have caused, but rather a sign of swelling in my brain.  She wanted to
rule out a brain tumour, so she scheduled an urgent CT Scan and referred me to a
neurologist for a consultation.

## CT Scan (Nov 16th)

Two days later, I went back to the hospital for my CT Scan. It was quick and
painless and revealed no brain tumour, which was a good sign, but I still didn't
know what was wrong. My vision started improving slightly, but I also started to
notice more irritation on my skin, which in fact, had started to spread to my
back and the bottom of my right foot. Until now, I hadn't even thought about the
skin irritation again as it had been so minor compared to the vision problems I
was experiencing. I now started to think that the two issues may be related.

## Visit to the Neurologist

My first visit to the Neurologist was at the brand new
[CHUM](https://www.chumontreal.qc.ca/) the University of Montreal's Hospital
Centre. The Notre Dame hospital was in the midst of a move to the new CHUM, and
some clinics had already been moved while others hadn't. Neurology was one of
the ones that had moved, so I got to be one of the early patients to check out
the super fancy new hospital. I gave the neurologist the story up to this point,
and mentioned the skin irritation as well as the eye issues. He told me my CT
scan had come back normal, which led him to think I might have elevated spinal
fluid pressure, causing the swelling in my optic nerve. The nerologist then performed a full neurologic exam He wanted to do a [lumbar
puncture](https://en.wikipedia.org/wiki/Lumbar_puncture) (spinal tap) to measure
my intracranial pressure.

## Nov 16th

- CT Scan - Nothing in your brain
- Vision starts improving slowly
- Skin irritation begins to spread, larger patch on chest, small patch on back,
  and bottom of foot
- First appointment with Neurologist
  - Neurologist performs lumbar puncture (spinal tap)
  - Finds increased pressure in cerebrospinal fluid (24)
  - Prescribes diamox (acetazolamyde) to lower cerebrospinal pressure
  - Requests an MRI (spinal column + brain)

## Nov ???

Follow up meeting with Neurologist

- Expected MRI results by now
- Analysis of cerebrospinal fluid showed traces of virus (which virus, is
  this accurate)?
- First started to notice numbness in feet Neuro tests with tuning fork
  confirm reduced sensation in right foot

## Dec 28th

- MRI
- My legs are very unstable. I walk to the MRI and back with help from the technician

## Jan 4th

- Go to emergency room. I can walk with a cane, but I fall down in triage.
- Admitted to hospital for 5 days
- First dose of corticosteroids

## Jan 5th - 8th

- daily dose of corticosteroids
- seen by a physical therapist
- legs are very numb, very little information coming and going

## Jan 10th

- made the doctor check my intestines, been constipated and freaking out about
  my bowels not working properly. Doctor said everything sounds normal, the guts
  seem to be working fine. So I forced myself to eat all my dinner and stop
  stressing out so much about not pooping.
- meeting with auto-immune specialist
  - mostly sure I have NMO
  - wants to eliminate Lyme's since the treatment for NMO (immune suppression)
    would not be good if I have Lyme's.
  - recommends plasmapheresis
  - positive recovery will be possible and that he has seen patient who were
    worse off than me recover fully

## Jan 12th

last night I got some porprioception back in my right leg. *Before* starting
plasmapheresis.  motor control of right leg starts getting better *before*
starting plasmapheresis start plasmapheresis treatment no drastic changes,
notice a feeling in my mouth while falling asleep that feels and sounds like I
have pop rocks in my mouth or that I'm carbonated and giving off little bubbles

## Jan 13 - 14th

- no treatments over the weekend
- do physical therapy exercises
- notice improvements in motor control of right leg, walking is improving slowly

## Jan 15th

- second plasmapheresis treatment
- no drastic changes
- still numbness fully surrounding my waist
- legs are still numb and lack feeling
- slightly more motor control of right foot
- walking is a little better (with walker still)

## Jan 16th

first attempt walking with cane, seems ok. I can balance and get around. Not as
easy as with the walker.

## Jan 18th

- 2nd Spinal Tap (lumbar puncture) to get more fluid to test for Lyme's
  disease. Since the blood work is taking so long, they decided to take some
  spinal fluid to do a PCR to look for Lyme's - Cerebro-spinal fluid pressure is
  back to normal at 14
- PICC line installed in right arm
- start antibiotics (Which ones?)
- tacos for dinner
- still waiting on Lyme's disease results from first blood test taken back in
  November

## Jan 19th

- erica + kids + mom and dad came by to visit with me
- 4th plasmapheresis treatment
- control and mobility are steadily improving, however
  numbness is still there, doesn't seem to be affected by the treatments
- maybe a tiny bit more feeling in my legs
- noticed I was uncomfortable sitting in one place after an hour -- I think
  that's new.
- Doctors say they will transfer me to a rehabilitation facility
  on Tuesday. I'm not sure that's a great idea since I'm not sure anyone knows
  what's going on with me and if the treatments are working.
- waiting on results of spinal tap fluid tests for Lyme's disease.

## Jan 22

- Final plasmapheresis treatment
- Doctor says I'll be on IV antibiotics for 2 weeks, then switch to pills
- Improvements on the toe feeling test and the "touche, piqué" test
- Physiotherapist says it's the best she's seen me walking yet
- Climbed 8 steps

## Jan 23

- discharged from hospital
- admitted to "centre de rehabilitation lindsay gingras" <https://ciusss-centresudmtl.gouv.qc.ca/nos-installations/centre-de-readaptation-en-deficience-physique-sensorielle-langage-auditive-et-motrice/installation-gingras-lindsay-de-montreal/>
  to start "readaptation" to normal life. This will consist of physio and
  occupational therapy.

## Jan 26

- OK to go home for weekend
- slow improvements with control and proprioception in right leg

## Jan 30

- Met with microbiologist said that second ELIZA for Lymes came back negative.
  Still no word on confirmation tests for *any* of my other Lyme's tests.
- Upgraded to walking w/out a walker, but with two canes!
- Erica and the kids came for lunch
- Tomorrow is my last IV antibiotic dose, and then they can take out the PICC line
- Legs still feel pretty numb, haven't noticed much improvement there lately
- Control of right leg is improving slowly
- Proprioception is still pretty bad
- Physical and Ergo therapy is going well.

## Feb 4

Spent the weekend at home, was pretty great. My legs were pretty tired, as well
as myself, but it was a great time. Felt so good to be home with Erica and the
kids.  On the way back to my room I ran into Javier from Mexico: I met a guy
from Mexico who's here. He got to Montreal in November and sometime between now
and then he put his boots on too tightly, his circulation got cut off and
possibly he got some frostbite. They had to amputate his big toe... but then
the infection kept spreading and they had to amputate the lower half of his
shin. He came to Canada to get a job and send money back to his wife and 4 kids
in Mexico.

My roommate Michael turned 75 today. He says lots of people came out for his
birthday, but he doesn't feel like he has much time left.  My legs and torso
are still numb. I had some itchiness on my belly and legs over the weekend,
which is maybe a good sign? Not sure.

Walking is getting better, i can get around pretty well w/out any extra help as
long as the ground is flat and stable.

## Feb 6

Today, met with neurologist (Dr Marchand). Did neuo tests. Seem to be
improving. Better distinction between being poked or touched, I can kinda feel
the vibrations, but they feel far away. Force tests are ok. Still feel a little
weaker on the right leg but not much. Neurologist said that my progress is
encouraging, and that it will likely take months for me to fully recover. He's
not convinced that I have NMO, but it's still a possibility. Still waiting on
results of Lyme's confirmation.

In physio did some balance tests on a machine that tells you how you're doing
balance-wise compared to the norm. I'm below the norm, but not by too much. I
definitely compensate with my vision right now. I performed the worst on the
tests that required me to close my eyes, or had visual movement but no actual
physical movement.

Mom and Dad brought the kids by for a quick visit, I was super tired after
physio.

## Feb 8

- More walking outside.
- Allowed to go up and down stairs at the centre by myself
- Graduated to walking inside with no help!
- Mom and dad came for a visit

## Feb 12

Went for a 40 minute walk outside today. That's more than what I do to go to
work every day, so I think walking to work will be doable. We'll try the STM
later this week to see how that goes. I did slip and fall on some ice, but that
was just at the beginning of the walk, I was being over-confident. I'm pretty
sure I can navigate the walk w/out issues, but when I first start going back to
work I'll take it slow and not do the whole walk right away.

Physiotherapist says she's going to recommend that I'm released this week,
Friday if possible. They have a meeting to go over my case this afternoon, and
she'll get back to me with what the decision is.

Weekend at home was great. Good time with Erica and the kids + the buelo's.
Took a show at home, and it went fine. No problems at all. I think I can do
almost all of the things I normally do at home... though the trash + recycling
are still going to be a problem until I'm a bit more comfortable on the stairs.

Took the stairs there and back for my trips to Ergo + Physio today. Went well,
but I'm still a little shaky on the stairs.

## Feb 14

Last night at the IGRLM.

Physiotherapist gave me some exercises to do at home Re-tested on balance
machine and gait trainer, improved in both. My left leg still travels less
distance than my right, but it's not horrible. Overall walking score is good.
Did better on the proprioception tests, but still not perfect. Especially had
trouble with mimicking the position of my left foot with my right foot. Better
than when I started at the IGRLM but still not normal.  Ergotherapist took me
out on the bus and metro to make sure I could handle public transportation. It
went pretty well. The bus was the hardest part, since I was standing and trying
to balance against the motion of the bus. Two people offered to let me sit
down, but I said no thanks, since I wanted to practice.  Symptoms appear to be
getting better, but it's still slow. I feel a bit more in my legs, but it's
still not the same as the feeling in my arms. It's encouraging that it's
getting better though.  Erica and the kids came for dinner at the caf. They
brought me valentine's day cards and some SNIKERDOODLES.

## March 6

Done one out patient physio session. Went well. I'm mostly back to normal by
now. Still have some lingering numbness in my body, still from the sternum
down. Saw the neurologist on the 23rd of Feb, he said that he was happy with my
progress and that he's still convinced that I do have NMO. He'd like me to
start on immunosuppressants. Back at work 3 days a week... going well so far,
getting back into the swing of things.

## July 1

Started taking Imuran Sunday July 1st. No changes after two days, but today I'm
feeling off. Was super tired this morning, and now am feeling a bit queasy. I
believe queasiness is one of the side effect, so it's possible that's related.

## July 7

Feeling better, about a week in to taking the medication. I had a small issue
wasn't feeling well and had a slight fever 101.1 a few days after starting the
medication. Not sure if it was related or not. I came home early from work,
went to bed and felt much better when I woke up.

## Dec 10

A year in.

Some lingering numbness and a feeling of my legs being asleep, seems to be more
noticeable at night as I'm going to bed.  I haven't tried any diet changes yet
to see if anything helps, but I think I may explore that more in the coming
year.

Had a follow up appointment with Dr Girard, he said things were looking good.
My MRI revealed some scarring, but he said that's to be expected due to the
severity of my attack. Continue taking the medication, and make sure I'm
getting my monthly blood tests done.

Follow up meeting scheduled for April (I think).

## May 8 2019

Been having a headache on one side of my head for about a week now. Mostly only
painful when I cough / sneeze. Not sure if it's just allergies or if it's NMO
related.

[[Shingles]]
