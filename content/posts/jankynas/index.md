+++
title = "JankyNAS"
date = "2024-01-23"
author = "Gabriel Guzman"
cover = ""
tags = ["NAS", "TrueNAS"]
keywords = ["", ""]
description = "Bringing my dead NAS back to life."
showFullContent = false
+++

![Image of JankyNAS](jankynas.jpg)

Once upon a time, I was playing with a hard drive magnet that slipped out of my
hand and landed on my laptop, directly above where the spinny hard drive was
housed. My laptop had been playing some music just before this happened, and
immediately stopped. I knew what I'd done almost before I noticed the problem and
my initial reaction was "oh shit.".   This was back before the SSD revolution
and spinning disks and magnets just don't mix, trust me. That computer was dead
in the water until I could replace the hard drive and reinstall it and get all
my documents restored from a backup (you have backups, right?). I'm not sure I
had backups back then but I definitely wish I did.

Instead of attempting to resuscitate my 14" MacBook Pro, possibly the favourite
computer I've ever owned, I ran to the nearest La Source, and bought a 400.00 PC
that I could install OpenBSD onto to get back up and running.  I was doing
freelance web work at the time, and no computer meant no money, so I needed to
get back up and running quickly and cheaply.

I bought one of these: ![Picture of an Acer Computer](acer.jpg)

As the hardware looked decent enough and it didn't have anything weird that
OpenBSD might complain about.

I used that computer happily for a few years until upgrading to a Dell XPS13
that I was also fairly certain could run OpenBSD. When I migrated off of the
Asus, I needed something to do with it, so I decided to turn it into a home NAS.
I'd been reading about FreeNAS and wanted to try it out, so I downloaded and
installed it. I bought two 2TB spinning disks (way more than I needed at the
time) and setup a ZFS mirrored pool, booting off a USB thumb drive, even though
that's not recommended by the FreeNAS (now TrueNAS) project.

This NAS served me well for several years, until one of the FreeNAS upgrades
wouldn't boot. I never took the time to dig deeply into what the problem might
be, I'd been using the NAS to host photos, music and movies I'd ripped from CD/DVDs
as well as for time machine backups for a Mac and a restic backup pool for my
OpenBSD laptop.

When it wouldn't boot anymore after the upgrade, I figured it was just a
transient issue and that it would be fixed upstream eventually, so I just
ignored the problem. Every few months I'd download a new version of FreeNAS and
install it to see if the system would boot again. What's interesting to note is
that the install disk always booted fine, but the boot drive would always crash.
I tried various different USB thumb drives even bought a few new ones just to be
sure it wasn't an issue with the thumb drives. I never bothered trying to
downgrade to an older version of FreeNAS since I assumed the problem wouldn't
last that long and getting this NAS back up and running was never really a
priority. It would be *nice* to get that data back, but it wasn't pressing.

Eventually FreeNAS became TrueNAS and still my issue persisted. When TrueNAS
Scale came out (a Linux based version of the original FreeBSD software) I was
like, this will definitely work now, but no dear reader, it did not.  I
continued to have the same problem where the install disk would always boot, but
the boot disk never would. I began to suspect that my issue was deeper than just
a software regression in the FreeBSD/Linux boot code and that perhaps the
hardware was at fault.

Getting the system back online still wasn't really a priority, though I did
realize that it had the only copy of most of the photos we'd taken of our son
when he was first born.

Lots of things happened in the interim, we moved, my XPS 13's battery ballooned,
I changed jobs, life got busy and I forgot about the NAS for a few years.

Finally, in 2023 while I was cleaning out my office I came across the NAS and
decided to actually put some time into it to see if I could get it to boot, or
if possible spin up the ZFS pool and get the data off.

I tried a few last times to get TrueNAS to install and the second time, instead
of the system freezing during the Linux boot, it just completely crapped out and
wouldn't even POST. I was left with a computer that wouldn't even turn on, no
matter what I tried. I'm pretty sure the issue was the power supply the whole
time and now it had finally given up the ghost.

Since I didn't have a spare power supply lying around, I looked around at my
assorted computer junk, and realized that my old XPS 13 with the battery that
was threatening to explode still worked fine, aside from the clear explosion
threat that it posed.

I opened it up, removed the battery balloon and hoped it wasn't one of those
laptops that needed the battery to be installed in order to power on. It wasn't,
and it turned on just fine, though it wouldn't boot.

I recalled that when I first installed OpenBSD on this laptop I'd had all kinds
of problems trying to boot until I'd managed to get into the BIOS setup (not
possible with an OpenBSD disk in the system) by removing the boot drive and
turning on the machine. I couldn't get to the BIOS unless I did this, I think
the machine had a hard time with what OpenBSD did to the boot sector of the
drive.

With the disk removed I was able to install TrueNAS to another USB thumb drive. I
pulled the old spinny disks out of my Acer and plugged them into a USB hub (also
not recommended by the TrueNAS folks). I'd envisioned having some difficulties
importing the ZFS pool, but to my great surprise it was as easy as clicking a
button in the TrueNAS GUI.

Just like that my data was back!  Running on my new JankyNAS, a laptop w/out a
battery and with only a USB thumb drive in one USB port and two drives and an
ethernet dongle plugged into a USB hub in the other.

The spinny disks appeared to be working pretty well, given they'd been offline
for ~5 years so I decided to make a few stability improvements to the system.

I formatted the original OpenBSD emmc drive, and disabled Intel Rapid Boot in
the BIOS (this is what had caused me booting problems on this laptop in the
past). Then I backed up the TrueNAS config, and reinstalled TrueNAS on the XPS
13 with the newly formatted emmc drive installed. This freed up one of the USB
ports, so I was able to do away with the USB hub and plugged the ethernet dongle
directly into one of the laptops USB ports (it only has 2) and the two drives
into the other.

I have one of these handy drive bays, so it was relatively easy to have both of
the drives plugged in at the same time.

![Image of disk drive bay](drivedock.jpg)

In the future, I'll want to get a better ethernet dongle as the one that's
currently installed is only capable of 100Mbps. I'll also replace the two spinny
disks with a single USB SSD. I think 4TB will probably be more than enough
as I'm not quite using all the 2TB I currently have available on the NAS. I
think that will keep me happy for awhile and then I'll be able to put the whole
thing into a closet somewhere and not have to see it on the floor of my office
anymore. Should be pretty quiet too, once I get rid of the spinny disks.

Since getting it back online, I've also backed up the photos that were on there
using restic so, at least I have backups of the photos now.  I haven't backed up
the DVDs yet, and I'm not sure I will as I can always re-rip those from the
original disks if I really care to.
