+++
title = "Managing engineers"
draft = true
+++

## Post ideas

### Managers shouldn't care about delivery

I know this may be a controversial statement, but it shouldn't be. An
engineering manager's primary responsibility should always be the team. You can't
serve both the team, *and* delivery the two objectives are at odds. Delivery
wants to extract as much as possible from the team without regard for their
wellbeing. A manager wants to optimze the teams wellbeing in order to maximize
their long term ability to deliver. It's a subtle but important difference. When
you serve delivery above all else, the team will suffer, people will leave,
quality will decline, and apathy will take hold.

In my ideal world view, the PO and the team own delivery, they are soley
responsible for dictating what the priorities are, what can be delivered, and
when it can be delivered. In a perfect world the engineering manager wouldn't
even know what was in the backlog. The engineering manager is responsible for
the wellbeing of the team and setting the stage for the team to do the best work
of their lives. What does this look like in practice.

As a manager I will never do any of the following:

- ask when will you be done with x?
- ask you to come in on Saturday.
- push you to work late to hit the fake deadline someone made up for you
- force you to be in a zoom call all day long while you're working just so I can
  see what you're up to.

As a manager I will always do most of the following:

- ask you if everything is going ok at home.
- ensure you're taking enough time off to remain effective at work.
- push back against outside influences pressuring you to work late.
- find out what you're good at and figure out how to get you doing that.
- find out what you hate and figure out how to make sure you can do less/none of
  it.
-

### Managing engineers

### What a great manager looks like

## Outline

- Engineers aren't fungilble resource units (FRUs) that you can just allocate to
  whichever project needs them.
  - Leads to brain drain - no one wants to work on a project they don't care
    about, using a stack they don't know.
  - Good engineers will leave

- Good leaders identify what people are good at, what they enjoy doing and then
  find or create a job that fits.
  - Hiring
    - What do I need? Platform, API, Web application, DevOPS tooling
  - Motivating
    - Tell a story
    - Purpose, Autonomy, Matery
  - Servant leadership
  - Alignment - make sure your leaders are aligned on what's important:
    - are we doing the right things?
  - Adjust - some teams may need more or less guidance, make sure you get them
    what they need.

- Treating engineers like humans goes a long way towards solving your
  engineering churn problems.
  - Let engineers decide what they want to work on
  - Let the best ideas attract the right talent
  - Make it easy for engineers to switch teams


- Good engineering leaders:
  - Work for the team
  - Enable the team
  - Empower the team
  - Trust the team

- Management
  - Managers shouldn't be accountable for the team's delivery
    - Managers main job is ensuring the team has an environment where they can
      do their best work. Being accountable for the delivery output of a team
      creates a situation where the manager has two competing masters -- teams
      delivery and teams health, if the manager is getting pressured to increase
      output on the team, team health will suffer.  If the team is healthy,
      output will increase.
    - Managers should be accountable to the team
    - Managers should be evaluated by their direct reports
    -
