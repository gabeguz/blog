+++
description = "My experience owning a Nest Protect"
categories = [
  "tech",
]
tags = [
  "reviews",
  "nest",
  "iot",
]
date = "2014-08-07T16:25:12-05:00"
title = "Nest Protect"

+++
Update
=======
After calling Nest support, they replaced my unit for me, no charge.  The new
unit arrived and has been sitting in it's place, behaving properly for a week
now.  We'll see how long it lasts.  Fingers crossed.

--

After about six months of faithful service, the [Nest
Protect](https://nest.com/smoke-co-alarm/life-with-nest-protect/) ended up with
all the other less intelligent smoke detectors:  On the table with the batteries
out.  

Let me back up.  About six months ago, I purchased a Nest Protect.  I like
gadgets, and a smart smoke detector seemed like a pretty cool gadget.  Setup was
a breeze, and before long it was mounted to the ceiling in my hallway.  Aside
from one incident involving over cooked bacon, I proceeded to forget the thing
was even there.  

About a week ago, I was at work, doing what I do (writing code and breaking
things) when suddenly my phone beeped and a pleasant notification told me there
was smoke in my apartment.   I thought it odd, and got a quick shot of
adrenaline as I pictured the tea pot on the stove and started replaying the
morning in my head.  Had I turned it off before leaving the house?  A flurry of
text messages later, I determined that my girlfriend and child were not at home,
so I called up my neighbour.  I let him know the situation and asked if he would
mind heading up to my apartment to make sure everything was ok.  

After a tense five minutes, I got the call back.  Everything was fine, and as
soon as he opened the door to the apartment the alarm said "Smoke is clearing"
and stopped blaring.  There was no smoke though, he informed me, no burning
smells either.   I relaxed, and went back to breaking things.  Not five minutes
later however, my phone alerted me again to smoke in my apartment.  WTF? I
thought to myself, and then I got a text from my neighbour saying he could hear
the alarm again,  and did I want him to go investigate?   

I said yes, he went up, and reported back.   "Everything OK, but alarm still
blaring."   At this point, I figured something was either wrong with the
detector, or there was a tiny fire burning in my apartment that no one could see
or smell.   I told him to just leave it, and I'd deal with it when I got home.
Eventually I got the "Smoke is clearing" alert again followed by yet another
"Smoke in the apartment" alert.  This repeated itself several times throughout
the day, until my girlfriend was able to take the alarm down off the ceiling.  

"As soon as I took it down, it said 'Smoke is Clearing' and stopped blaring" she
told me as I got home from work.  "I put it on your desk" she said.  By this
time, I'd already done the requisite googling, and learned that it's possible
that dust was causing the sensor to misbehave.  Since I didn't have any
compressed air however, I just left the device on my desk.  Maybe something is
going on up near the ceiling I thought to myself.  The Protect behaved itself
until right around 2am when it started going off again "Smoke in the apartment
--- BEEP, BEEP, BEEP --- Smoke in the apartment."  

I got up, found the Protect, pulled out the batteries and went back to sleep.  


Addendum
========
I haven't been able to find many people who've had this same problem, and I
still haven't had a chance to follow the 'de-dusting' procedure outlined on
Nest's website.  I did attempt to call their customer support line.  Maybe they
should rename it their sales line, because I was unable to find an option that
would allow me to ask for help with my actual device. 

I also have a [birdi](http://getbirdi.com) smoke detector with my name on it, so once they start shipping to their indiegogo backers, I'll be able to see if it fares any better than the Nest.  
