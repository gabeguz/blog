+++
categories = [
  "random",
]
tags = [
  "blogging",
  "web",
  "zope",
  "plone",
]
date = "2017-02-06T12:32:48-05:00"
title = "A New Home"
description = "A note about moving to lifewaza.com"
+++

I've been "on the net" since 1997.  A lot has changed in that time, and a lot
has stayed the same.  I've tried blogging platforms with bells and whistles, but
always come back to simple hand-edited pages. I think, now I've found the best
system that works for me.  Staic site generators that allow you to write in
markdown using the text editor of your choice, and then convert your prose to
html ready for publishing.  I've never found a web editor that I enjoyed using
as much as I like typing in the terminal (xterm + vim, ftw), and while
hand-coding html is just fine, sometimes it can be a bit tedious.   The advent
of 'structured text' like [markdown][11] and [restructured text][10] also make things pretty
easy now.  You can have a file that looks good in your text editor and also
looks good on the web.

Since, I've had "content" online since 1997, I decided to see if I could find it
and bring it all together under one roof.  In the process, I found lots of old
posts that I had completely forgotten about, and decided to re-post them here.
I've culled a fair amount of the junk, and only brought the stuff that I still
find interesting.  You can still find the junk out there if you really want to.
I've preserved the original post dates where they existed, and have only edited
the content so it looks fine in markdown.  I haven't changed any of the words
(except where needed to protect the innocent).

It was really nice to revisit 20 years of my life as seen through my own
thoughts and words, and get a chance to relive some of my experiences.

Past homes (most recent first) 
==============================

- [dev.to/gabeguz][18] dev.to is probably my favourite blogging platform so far.
  I'll likely continue to syndicate my dev centric content there for awhile.

- [medium][19] I had a brief stint on medium, but I disagree fundamentally with
  their walled garden approach so I closed my account.

- [gabe.svbtle.com][4] I wanted to try out svbtle, as I liked the clean look
  and a few people who's blogs I followed were there.  It's nice overall, no
  nonsense and stays out of your way so you can write.  The things I didn't like
  so much are that it's not open source, and there's no easy way to get your posts
  *out* of the system.

- [guzman-nunez.com][3]  After trying the database backed dynamic blog thing
  for a time, I decided I didn't need all the bells and whistles and so just went
  back to hand-editing my site.  I didn't need much in the way of formatting, and
  plain old html was more than adequate for my needs.  Markdown et al didn't exist
  yet, or I'm sure I would have used that.  

- [devnull][5] - I went through a few different blogging/publishing platforms here, most
  notably [plone][1], and [zope][2].  Surprisingly, I never tried one of the php
  backed blogging platforms even though by this time I was mostly doing php at
  my dayjob.  While they were all fine at what they did, I didn't really need
  the complexity for a simple blog, and the ocasiaonal posts
  about travelling.
  
- [www.acusd.edu '97][6] and [www.acusd.edu '01][9] - My first home on the internet.  This was all hand edited
  html, a few simple pages with text and some pictures.   I didn't really use the
  space much, but I had a few book lists and some articles I wrote for humor
  hosted here as well as some papers I had written for school.  This is where I
  first learned about [vi(1)][7] and [emacs(1)][8], and cgi-bin and public_html and ~username
  addresses.  My first real usage of a unix like system (Solaris).


New Home
========
My new home is, as you've probably guessed, [here][12]. This particular site is
written in [markdown][11] with [ViM][13], converted to html with [Hugo][14] and hosted on a VPS at
[Vultr][15] running [OpenBSD][16] and the [httpd(8)][17] webserver.

[1]: https://plone.org/		"Plone"
[2]: http://old.zope.org/	"Old Zope"
[3]: http://web.archive.org/web/20070707032610/http://www.guzman-nunez.com/ "guzman-nunez.com"
[4]: http://gabe.svbtle.com/	"gabe.svbtle.com"
[18]: https://dev.to/gabeguz "dev.to/gabeguz"
[5]: http://web.archive.org/web/20070707032531/http://www.guzman-nunez.com/gabe/devnull "devnull"
[6]: http://web.archive.org/web/19970714002016/http://www.acusd.edu/~guzman/ "acusd.edu"
[7]: http://man.openbsd.org/OpenBSD-current/man1/vi.1
[8]: https://www.gnu.org/software/emacs/
[9]: http://web.archive.org/web/20010515192946/http://www.acusd.edu/~guzman/
[10]: https://en.wikipedia.org/wiki/ReStructuredText
[11]: https://en.wikipedia.org/wiki/Markdown
[12]: http://www.lifewaza.com/ "lifewaza"
[13]: http://vim.org
[14]: http://gohugo.io/
[15]: http://www.vultr.com/?ref=6819733 "vultr"
[16]: http://www.openbsd.org
[17]: http://man.openbsd.org/OpenBSD-current/man8/httpd.8
[19]: https://medium.com
