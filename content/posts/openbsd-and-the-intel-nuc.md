+++
description = "A report of running OpenBSD on an Intel NUC"
categories = [
  "tech",
]
tags = [
  "OpenBSD",
  "unix",
]
date = "2014-07-28T17:40:12-05:00"
title = "OpenBSD and the Intel NUC"

+++
**Update - A reader pointed out that the intel i5-4250U Processor only has 2
cores and not 4.  I've changed the article to reflect that.  It has 4 "threads"
and so the OS sees 4 processors, but in fact it only has 2 cores.  Thanks to
Jeff for pointing that out.**

While shopping around for a small form factor PC, I came across the [Intel
NUC](http://www.intel.com/content/www/us/en/nuc/overview.html).   It was the
machine our IT department had decided to use to drive all the 50" TV's that they
had put up all over the office.  I was initially drawn to its tiny size, and
neat look.  It's a very clean looking box.   

Although the NUC is a tiny computer, it's packed with power.  The model I
purchased has a 1.3GHz i5 with two cores.  I added 16GB's of RAM (the maximum)
and a 250GB mSATA SSD.  The NUC comes standard with gigabit Ethernet and four
USB 3.0 ports.  There is also a mini PCI Express slot for adding wifi, if
wanted.  Since the NUC was going to be living on my desk, I decided against the
wifi for now.   The NUC has integrated Intel graphics (Intel® HD Graphics 5000)
which as an OpenBSD user is exactly what I wanted.   It's also capable of
driving a high resolution display, and since I had recently acquired one of the
beautiful [Monoprice 27" IPS 2560 x 1440
displays](http://www.monoprice.com/Product?p_id=10509&format=2) from
[Massdrop](https://www.massdrop.com/r/UU4DTT)\* it was a perfect fit.  

There was one small hiccup, when it came time to connect the display to the NUC,
I realized that the display accepted dual link DVI, and the NUC presented mini
HDMI or mini Display Port only.  Off to the interwebs, I found a [mini Display
Port to Dual-Link DVI
adapter](http://www.amazon.ca/Startech-Com-MDP2DVID-Display-Dual-Link-Powered/dp/B004I6L6DW%3FSubscriptionId%3DAKIAILSHYYTFIVPWUY6Q%26tag%3Dduckduckgo-d-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB004I6L6DW),
which is USB powered.  

Once the adapter arrived, everything was in place and I was able to boot over
the network via
[pxeboot(8)](http://www.openbsd.org/cgi-bin/man.cgi?query=pxeboot&apropos=0&sec=0&arch=default&manpath=OpenBSD-current)
and install [OpenBSD](http://www.openbsd.org).

As usual, the installation was fast and straightforward.   One of the things I
like most about OpenBSD is their no frills installer.  It's a simple text based
program that has sane defaults (for installing OpenBSD at least).  Once you have
used it a few times, installation is as easy as hitting return over and over
again until you are done.  The other nice thing about the install is that it is
fast.  Downloading the full OS and installing it takes me about 10 minutes on
average.   The great thing also is that this has been steady since I started
using OpenBSD way back when.  

After several months of break-in, I can now comfortably say that OpenBSD runs
great on the NUC.  I have no problem streaming 1080p video from youtube (html5
only -- no flash for me), or playing them directly over my LAN.  VLC and mplayer
work great, as does gnome3.  I don't play any games so I can't say how they
work, but as a standard desktop system, the machine works great.

Like a good OpenBSD user, I include my
[dmesg(8)](http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man8/dmesg.8?query=dmesg)
below.   

\*That's a referral link, if you don't like th\at, just go to massdrop.com directly.  

```
    OpenBSD 5.6-beta (GENERIC.MP) #304: Fri Jul 25 12:02:01 MDT 2014  deraadt@amd64.openbsd.org:/usr/src/sys/arch/amd64/compile/GENERIC.MP
    real mem = 17088114688 (16296MB)
    avail mem = 16624447488 (15854MB)
    mpath0 at root
    scsibus0 at mpath0: 256 targets
    mainbus0 at root
    bios0 at mainbus0: SMBIOS rev. 2.8 @ 0xec130 (84 entries)
    bios0: vendor Intel Corp. version "WYLPT10H.86A.0021.2013.1017.1606" date 10/17/2013
    bios0: Intel Corporation D54250WYK
    acpi0 at bios0: rev 2
    acpi0: sleep states S0 S3 S4 S5acpi0: tables DSDT FACP APIC FPDT LPIT SSDT SSDT MCFG HPET SSDT SSDT DMAR CSRT
    acpi0: wakeup devices PS2K(S3) PS2M(S3) PXSX(S4) RP01(S4)     PXSX(S4) RP02(S4) PXSX(S4) RP03(S4) PXSX(S4) RP04(S4) PXSX(S4) RP05(S4) PXSX(S4) RP06(S4) PXSX(S4) RP07(S4) [...]
    acpitimer0 at acpi0: 3579545 Hz, 24 bits
    acpimadt0 at acpi0 addr 0xfee00000: PC-AT compat
    cpu0 at mainbus0:        apid 0 (boot processor)
    cpu0: Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz, 2295.07 MHz
    cpu0: FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CFLUSH,DS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE,SSE3,PCLMUL,DTES64,MWAIT,DS-CPL,VMX,EST,TM2,SSSE3,FMA3,CX16,xTPR,PDCM,PCID,SSE4.1,SSE4.2,x2APIC,MOVBE,POPCNT,DEADLINE,AES,XSAVE,AVX,F16C,RDRAND,NXE,PAGE1GB,LONG,LAHF,ABM,PERF,ITSC,FSGSBASE,BMI1,AVX2,SMEP,BMI2,ERMS,INVPCID
    cpu0: 256KB 64b/line 8-way L2 cache
    cpu0: smt 0, core 0, package 0
    mtrr: Pentium Pro MTRR support, 10 var ranges, 88 fixed ranges
    cpu0: apic clock running at 99MHz
    cpu0: mwait min=64, max=64, C-substates=0.2.1.2.4, IBE
    cpu1 at mainbus0: apid 2 (application processor)
    cpu1: Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz, 2294.69 MHz
    cpu1: FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CFLUSH,DS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE,SSE3,PCLMUL,DTES64,MWAIT,DS-CPL,VMX,EST,TM2,SSSE3,FMA3,CX16,xTPR,PDCM,PCID,SSE4.1,SSE4.2,x2APIC,MOVBE,POPCNT,DEADLINE,AES,XSAVE,AVX,F16C,RDRAND,NXE,PAGE1GB,LONG,LAHF,ABM,PERF,ITSC,FSGSBASE,BMI1,AVX2,SMEP,BMI2,ERMS,INVPCID
    cpu1: 256KB 64b/line 8-way L2 cache
    cpu1: smt 0, core 1, package 0
    cpu2 at mainbus0: apid 1 (application processor)
    cpu2: Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz, 2294.69 MHz
    cpu2: FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CFLUSH,DS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE,SSE3,PCLMUL,DTES64,MWAIT,DS-CPL,VMX,EST,TM2,SSSE3,FMA3,CX16,xTPR,PDCM,PCID,SSE4.1,SSE4.2,x2APIC,MOVBE,POPCNT,DEADLINE,AES,XSAVE,AVX,F16C,RDRAND,NXE,PAGE1GB,LONG,LAHF,ABM,PERF,ITSC,FSGSBASE,BMI1,AVX2,SMEP,BMI2,ERMS,INVPCID
    cpu2: 256KB 64b/line 8-way L2 cache
    cpu2: smt 1, core 0, package 0
    cpu3 at mainbus0: apid 3 (application processor)
    cpu3: Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz, 2294.69 MHz
    cpu3: FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CFLUSH,DS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE,SSE3,PCLMUL,DTES64,MWAIT,DS-CPL,VMX,EST,TM2,SSSE3,FMA3,CX16,xTPR,PDCM,PCID,SSE4.1,SSE4.2,x2APIC,MOVBE,POPCNT,DEADLINE,AES,XSAVE,AVX,F16C,RDRAND,NXE,PAGE1GB,LONG,LAHF,ABM,PERF,ITSC,FSGSBASE,BMI1,AVX2,SMEP,BMI2,ERMS,INVPCID
    cpu3: 256KB 64b/line 8-way L2 cache
    cpu3: smt 1, core 1, package 0
    ioapic0 at mainbus0: apid 2 pa 0xfec00000, version 20, 40 pins
    acpimcfg0 at acpi0 addr 0xf8000000, bus 0-63
    acpihpet0 at acpi0: 14318179 Hz
    acpiprt0 at acpi0: bus 0 (PCI0)
    acpiprt1 at acpi0: bus -1 (PEG0)
    acpiec0 at acpi0: not present
    acpicpu0 at acpi0: C2, C1, PSS
    acpicpu1 at acpi0: C2, C1, PSS
    acpicpu2 at acpi0: C2, C1, PSS
    acpicpu3 at acpi0: C2, C1, PSS
    acpipwrres0 at acpi0: FN00, resource for FAN0
    acpipwrres1 at acpi0: FN01, resource for FAN1
    acpipwrres2 at acpi0: FN02, resource for FAN2
    acpipwrres3 at acpi0: FN03, resource for FAN3
    acpipwrres4 at acpi0: FN04, resource for FAN4
    acpitz0 at acpi0: critical temperature is 105 degC
    acpitz1 at acpi0: critical temperature is 105 degC
    acpibat0 at acpi0: BAT0 not present
    acpibat1 at acpi0: BAT1 not present
    acpibat2 at acpi0: BAT2 not present
    acpibtn0 at acpi0: PWRB
    acpibtn1 at acpi0: LID0
    acpivideo0 at acpi0: GFX0
    acpivout0 at acpivideo0: DD1F
    cpu0: Enhanced SpeedStep 2295 MHz: speeds: 1901, 1900, 1800, 1700,    1600, 1500, 1400, 1300, 1200, 1100, 1000, 900, 800, 779 MHz
    pci0 at mainbus0 bus 0
    pchb0 at pci0 dev 0 function 0 "Intel Core 4G Host" rev 0x09
    vga1 at pci0 dev 2 function 0 "Intel HD Graphics 5000" rev 0x09
    intagp at vga1 not configured
    inteldrm0 at vga1
    drm0 at inteldrm0
    drm: Memory usable by graphics device = 2048M
    error: [drm:pid0:i915_write32] *ERROR* Unknown unclaimed register before writing to 100000
    inteldrm0: 2560x1440
    wsdisplay0 at vga1 mux 1: console (std, vt100 emulation)
    wsdisplay0: screen 1-5 added (std, vt100 emulation)
    azalia0 at pci0 dev 3 function 0 "Intel Core 4G HD Audio" rev 0x09: msi
    azalia0: No codecs found
    "Intel 8 Series xHCI" rev 0x04 at pci0 dev 20 function 0 not configured
    "Intel 8 Series MEI" rev 0x04 at pci0 dev 22 function 0 not configured
    em0 at pci0 dev 25 function 0 "Intel I218-V" rev 0x04: msi, address  c0:3f:d5:62:28:cd
    azalia1 at pci0 dev 27 function 0 "Intel 8 Series HD Audio" rev 0x04: msi
    azalia1: codecs: Realtek/0x0283
    audio0 at azalia1
    ehci0 at pci0 dev 29 function 0 "Intel 8 Series USB" rev 0x04: apic 2 int 23
    usb0 at ehci0: USB revision 2.0
    uhub0 at usb0 "Intel EHCI root hub" rev 2.00/1.00 addr 1
    pcib0 at pci0 dev 31 function 0 "Intel 8 Series LPC" rev 0x04 
    ahci0 at pci0 dev 31 function 2 "Intel 8 Series AHCI" rev 0x04: msi, AHCI 1.3
    scsibus1 at ahci0: 32 targets
    sd0 at scsibus1 targ 3 lun 0: <ATA, Crucial_CT240M50, MU03> SCSI3 0/direct fixed naa.500a0751094f2f7d
    sd0: 228936MB, 512 bytes/sector, 468862128 sectors, thin
    ichiic0 at pci0 dev 31 function 3 "Intel 8 Series SMBus" rev 0x04: apic 2 int 18
    iic0 at ichiic0
    spdmem0 at iic0 addr 0x50: 8GB DDR3 SDRAM PC3-12800 SO-DIMM
    spdmem1 at iic0 addr 0x52: 8GB DDR3 SDRAM PC3-12800 SO-DIMM
    isa0 at pcib0
    isadma0 at isa0
    com0 at isa0 port 0x3f8/8 irq 4: ns16550a, 16 byte fifo
    pckbc0 at isa0 port 0x60/5
    pcppi0 at isa0 port 0x61
    spkr0 at pcppi0
    lpt0 at isa0 port 0x378/4 irq 7
    wbsio0 at isa0 port 0x4e/2: NCT6776F rev 0x33
    lm1 at wbsio0 port 0xa00/8: NCT6776F
    fdc0 at isa0 port 0x3f0/6 irq 6 drq 2
    fd0 at fdc0 drive 0: density unknown
    fd1 at fdc0 drive 1: density unknown
    uhub1 at uhub0 port 1 "Intel Rate Matching Hub" rev 2.00/0.04 addr 2
    uhub2 at uhub1 port 1 "NEC hub" rev 2.00/1.00 addr 3
    uhidev0 at uhub2 port 2 configuration 1 interface 0 "Logitech USB Optical Mouse" rev 2.00/63.00 addr 4
    uhidev0: iclass 3/1
    ums0 at uhidev0: 3 buttons, Z dir
    wsmouse0 at ums0 mux 0
    uhidev1 at uhub1 port 2 configuration 1 interface 0 "Metadot - Das Keyboard Das Keyboard Model S" rev 1.10/1.00 addr 5
    uhidev1: iclass 3/1
    ukbd0 at uhidev1: 8 variable keys, 6 key codes
    wskbd0 at ukbd0: console keyboard, using wsdisplay0
    uhidev2 at uhub1 port 2 configuration 1 interface 1 "Metadot - Das Keyboard Das Keyboard Model S" rev 1.10/1.00 addr 5
    uhidev2: iclass 3/0, 3 report ids
    uhid0 at uhidev2 reportid 2: input=1, output=0, feature=0
    uhid1 at uhidev2 reportid 3: input=3, output=0, feature=0
    vscsi0 at root
    scsibus2 at vscsi0: 256 targets  
    softraid0 at root
    scsibus3 at softraid0: 256 targets
    root on sd0a (6938ad82731e8a13.a) swap on sd0b dump on sd0b
```
