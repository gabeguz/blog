+++
title = "OpenBSD and the pcengines APU2"
draft = true
+++

## pcengines APU2

Ever since soekris engineering stopped making their OpenBSD friendly devices
I've been using the pcengines APU boards as home routers. I currently have an
older APU unit with OpenBSD 6.0 that I've been meaning to upgrade.


## Serial console

Since there's no video port on the APU2, you'll need a serial cable to connect
to the DE9 port on the box. I use one of [these](). Plug the USB end into
another computer and the DE9 end into the APU2. From the other computer, use the
serial communication program of your choice, for me, on OpenBSD that's `cu`: 

```
$ cu /dev/xxx 115200

```

Plug in the APU2 and you should see boot messagse in your terminal window. 

## Network boot

For the next part, we're going to have to configure a few things.

### TFTP

On another machine, you'll need to enable `tftpd(1)` the (t)rivial (f)ile
(t)ransfer (p)rotocol (d)aemon. This is ... and is what the APU2 will use to
load the BSD kernel.

### DHCPD

[What is DHCP?](). You'll need to tell your dhcp server that it should also
advertise pxeboot.`

### Configure a boot.conf

In order to properly boot the APU2, you'll need to tell OpenBSD that it needs to
use the com port as it's primary TTY. You can do that either by typing this when
the OpenBSD boot prompt appears: 

```
stty com0 115200
set tty com0
```
Or by creating a boot.conf with the same lines in it in the same directory as
you put the bsd.rd file in the TFTP section of this article. 

## Install OpenBSD

You should now see the familiar OpenBSD installer.
