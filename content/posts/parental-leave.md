+++
tags = [
  "",
  "",
]
date = "2017-02-10T20:50:56-05:00"
title = "parental leave"
description = ""
draft = true
categories = [
  "",
]

+++

## Parental Leave

In Quebec, residents are entitled to a generous parental leave program. When I had each of my two children the benefit could be broken down as follows: 

5 weeks for the father

18 weeks for the mother

32 weeks that could be split between the father and the mother however you wanted.

During your leave, your employer must keep your job for you, and you recieve up to 70% of your salary during your leave (paid by the government).

