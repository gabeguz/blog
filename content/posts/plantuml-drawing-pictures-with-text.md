+++
title = "plantuml - drawing pictures with text"
description = "First impressions of a tool that allows you to generate UML from text."
tags =  ["diagrams", "architecture", "UML", "tools"]
date = "2017-05-24"
+++

Someone at work recently pointed me towards [PlantUML](http://plantuml.com/), a really neat tool that takes a simple textual description like this: 

```
me -> you: Say "Hello"
you ->me: Say "Hi" back
me -> you: Ask "Do you like cookies?"
you -> me: Reply "Of Course!"
```

And turns it into a nice diagram, like this: 

![rendered plantuml diagram](https://i.imgbox.com/WpOXbv2m.png)

It supports a wide range of UML and non UML diagrams (sequence, class, state, object, etc) and is fairly easy to read in its textual form.  

It's way easier than going into google draw, and making boxes and then drawing arrows and lining everything up.  Just change your text file, and then re-render your image and you are good to go.   I've been using this for modeling all kinds of things at work, and I don't think I'll ever go back!

PlantUML is opensource and it's integrated with a lot of the tools you probably already use (see: [plantuml.com/running](http://plantuml.com/running)).
