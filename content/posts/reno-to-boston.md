+++
title = "Road Trip: Reno to Boston"
description = "A log of a trip across the USA, from Reno, NV to Boston, MA"
categories = [
  "adventure",
]
tags = [
  "travel",
  "road trip",
  "reno",
  "boston",
  "boulder",
  "chicago",
  "NY",
  "MA",
  "WY",
  "NV",
  "CO",
  "IA",
  "IL",
]
date = "2007-01-31T22:32:35-05:00"

+++

[Picture of the sun rising in the Nevada desert] I've known for about 6 months
that I'd be heading North to Canada to help my friend Karl Grignon with his new
dojo. My plans have fluctuated greatly since then. Initially, I thought I might
like to drive to Montreal from Reno spending a few weeks on the road, visiting
the friends I have scattered across the country. Then I thought perhaps I should
just leave my truck with my parents and fly to Montreal so I wouldn't have to
worry about having a car in a city where parking is scarce. The next plan
involved driving north to Vancouver, BC and then driving across Canada to
Montreal, seeing whatever there was to see. This plan was admittedly not very
well thought out as driving across Canada in the dead of winter would probably
pose some technical difficulties I wasn't prepared to deal with. Several friends
had also mentioned more than a passing interest in road tripping with me, so the
car started to look like more and more of a possibility.

[Picture of some cool looking clouds in the Nevada desert] Slowly, the following
plan developed. My friend Erica had mentioned that she was planning on moving
out to Boston around the beginning of 2007. That just happened to mesh with when
I was planning on heading to Canada. We talked about road tripping about a month
before I went to Peru and figured if things worked out, we'd road trip together,
and if not, well, I was probably still driving to Canada at this point, so I'd
find someone else. Over the course of the next few months, we decided on
Interstate 80, with a detour to Boulder, CO to train with Hiroshi Ikeda Sensei
of Boulder Aikikai.

[Picture of my truck with a u-haul trailer attached] The week leading up to our
trip, I ironed out the last of the logistics, got a U-Haul trailer for the
truck, helped Erica get her apartment into boxes, and did just about zero actual
planning as far as driving was concerned. The two of us sat down for about 10
minutes looking at a map, making comments like: "If we leave on Monday, and go
as far as we can, we can probably get to Boulder on Tuesday, then drive as far
as we can on Wednesday." "Wait, we need to see my Uncle on in Des Moines on
Wednesday." "Right... so drive to Des Moines on Wednesday, then Chicago on
Thursday (Erica's uncle is there), as far as we can on Friday, and hopefully get
to Boston on Saturday." Is that right? Well, it went something like that, and
that's pretty much the extent of the planning we did.

Sure enough, Tuesday, January 30th came around and we were on the road.
Amazingly, we pretty much managed to stick to the 'plan.'

[Picture of a large rock formation in CO, I think]

- 1/30/2007 Reno, NV - Little America, WY (662 miles)
- 1/31/2007 Little America, WY - Boulder, CO (375 miles)
- 2/01/2007 Boulder, CO - Des Moines, IA (687 miles)
- 2/02/2007 Des Moines, IA - Chicago, IL (334 miles)
- 2/03/2007 Chicago, IL - Findley Lake, NY (460 miles)
- 2/04/2007 Findley Lake, NY - Boston, MA (537 miles)

3,055 miles of road trip goodness.

[Picture of me driving] Did I mention we had 2 cats with us? Erica has 2 cats,
Miles Davis and Rosalind (aka Rosie). The two kitties are named after the jazz
genius, and the little known scientist who was integral in Watson and Crick's
discovery of the structure of DNA, respectively. Miles and Rosie are great
kitties, though they aren't so fond of driving across the country. I guess I
wouldn't be too happy if I were stuck in a little box all day long for 5 days;
no use pointing out to me that I was stuck in a little box (albeit with wheels)
all day long for 5 days.

The kitties actually behaved pretty well given the circumstances. They pretty
much stayed in their carrier (we had an extra large cat/dog mesh "tent"
basically) huddled together, hoping the trip would be over soon. I imagine that
if we could have peeked into their thoughts, we would have seen something like
this: 

 - Miles: Are we there yet? Are we there yet? Are we there yet? What the hell
is going ON? I have to pee. I hate life. Don't touch me. Are we there yet? Are
we there yet? Are we there yet?
 - Rosie: This sucks.

Not that I blame them. That much driving gets on your nerves.

So, what were the highlight of the trip? For me, the following things stood out
as memorable:

    Picture of a billboard leading up to the Little America hotel in Wyoming
    Getting to See Ikeda Sensei teach a class
    Driving through Wyoming
    The Bonneville Salt flats
    Rosie going crazy and attacking Erica when she tried to put her back into the carrier on day 2
    Little America (nano America? minute America? one hotel on the side of the interstate America) WY and all the billboards leading up to it
    Erica saying to me: "I just cut your seat."
    Jackknifing the trailer into a snowbank in NY
    Seeing Tani and Gracia in Boulder
    Seeing my Uncle in Des Moines, and Erica's Uncle in Chicago
    Waking up at 4am the first day of our trip
    Sunrise over the Nevada desert
    Sunset everywhere we saw it
    The "I LOVE SODOMY" sign in the middle of Nebraska
    Eating Tapas in Chicago
    Meeting Joe and Nan in Boston

[Picture of sunset in Utah, taken by Erica.]

Overall, I'd say I had a pretty good time, even though at times I'd have happily
been done with the trip. No more road trips in 2007, at least not of any
distance more than 300 miles. 
