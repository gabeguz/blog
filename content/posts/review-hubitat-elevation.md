+++
title = "Review of the hubitat elevation IoT hub"
date = "2020-10-25"
tags = [ "review", "hubitat", "IoT", "zigbee", "zwave" ]
categories = [ "reviews", "tech" ]
+++

I've been playing with home automation for a few years now and wanted to stop relying on 3rd party cloud software for the devices I run at home. I also wanted to stop connecting my IoT devices to my wifi in order to control them. In order to do this, I needed a zigbee or z-wave hub. One of my friends has a [hubitat elevation](https://www.amazon.ca/gp/product/B07D19VVTX?ie=UTF8&tag=gabeguz-20&camp=15121&linkCode=xm2&creativeASIN=B07D19VVTX) and recommended it it to me so I ordered one to try it out.


## First impressions

It's so small! I was expecting something the size of an Intel Nuc, or maybe a Raspberry Pi, but the hubitat is truly tiny. Setup was easy, just plug in an ethernet cable and power and you're good to go. 

## Integration with Home Assistant

There are two ways I've found to integrate the hubitat with home assistant, though I haven't been able to confirm if either works yet. Since I can't seem to get my one zigbee device to pair with the hubitat, I can't tell if it's going to show up in home assistant once it's paired.

Update: Managed to get MQTT working by using [this project](https://github.com/xAPPO/MQTT). All devices that the hubitat can see are now showing up in home assistant and I can control them from there. I had to get a few more zigbee devices in order to extend the range of my zigbee network. It turns out that putting a zigbee radio into what's effectively a [Faraday cage](https://en.wikipedia.org/wiki/Faraday_cage)(a metal electrical gang box) isn't great for the signal.  

### MQTT

Ideally, I'd like to go the MQTT route. I already have an [MQTT broker](https://mosquitto.org/) running at home and I'd like to leverage that. I found some docs on how to set this up (as it's not officially supported by the hubitat) and followed them and everything seems to be working. I can't know for sure until I get a device to pair with the hubitat and see if I can get a message to home assistant. 

Update: I got this working, as described above.

### Maker API

If the MQTT route doesn't work, there's a home assistant integration (a community integration, not an official one) that works with hubitat's Maker API. I haven't had a chance to try this yet, but if I can't get any results with the MQTT setup, I'll try this next.

## Would I buy this again

To early to tell. So far it seems to be working fine, though I haven't been able to pair it with the one piece of zigbee equipment I own. I'll update this review when I learn more.

Update:

Yes, it seems to meet my needs.
