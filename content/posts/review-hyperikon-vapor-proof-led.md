+++
title = "Review of the hyperikon vapor proof LED fixture"
date = "2020-10-25"
tags = [ "review", "lights", "IoT", "hyperikon" ]
categories = [ "reviews" ]
+++

## What do I need this for?

The lights in my duck house aren't quite bright enough to cover the space adequately. The duck house is also a high humidity, high dust environment. We currently have 19 chickens and 10 ducks sleeping in there (they free range during they day). Before getting these, I was using two Lifx BR30 bulbs which overall worked fine, but had a few drawbacks: 

1. They are wifi only
2. If the power goes out they turn back on
3. Not meant for harsh environments

Eventually I may write a [review of those](/posts/review-lifx-br30.md) but not today.

## First Impressions

The [hyperikon light fixtures](https://www.amazon.ca/gp/product/B01ENWKO92?ie=UTF8&tag=gabeguz-20&camp=15121&linkCode=xm2&creativeASIN=B01ENWKO92) are great! Realy nice light and very bright. I put two in the duck house which seems to be just right for that space.

## Installation

Installation was pretty easy, though these lights don't plug into a socket, they need to be wired into your existing lighting fixture. I'd never done this before, but the basic process was:

0. Turn off the power at the breaker!!!
1. Remove the old light
2. Open the junction box
3. Connect the live, neutral, and ground wires to the new light
4. Close everything up
5. Turn on the power

## Would I buy this again?

Yes, absolutely.

I bought a 4 pack, figuring I'd use two in the duck house and two in my garage to replace some old flourescent tubes. I like them so much, I think I'm going to get two more for the garage so I can get rid of all the old lights in there. Great light and I'm really happy that they are sealed and should hold up well to the abuse of my flock.
