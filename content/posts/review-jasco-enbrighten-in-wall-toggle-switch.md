+++
title = "Review of the Jasco Enbrighten in wall toggle switch"
date = "2020-10-25"
tags = [ "IoT", "review", "tech", "zigbee", "smartswitch" ]
categories = [ "reviews" ]
+++

As part of my [move away from cloud based IoT devices](/posts/review-hubitat-elevation) I've been looking for a zigbee switch I could use to automatically turn the lights in my [duck house]() on and off. I got the [hubitat](https://www.amazon.ca/gp/product/B07D19VVTX?ie=UTF8&tag=gabeguz-20&camp=15121&linkCode=xm2&creativeASIN=B07D19VVTX) on a recomendation from a friend and managed to find [these zigbee toggle switches](https://www.amazon.ca/gp/product/B08428P2MC?ie=UTF8&tag=gabeguz-20&camp=15121&linkCode=xm2&creativeASIN=B08428P2MC) that should work with it.  After spending a few hours rewiring some of the electrical in the duck house (I'm *not* an electrician) and managing to not burn it down, I flicked the switch on and the [lights](/posts/review-hyperikon-led-fixture) came on!

## Installation

Installation was pretty straightforward given that 1) I'm not an electrician and 2) I've never installed a wall switch before. The included documentation was clear and with that and a few youtube videos I was able to get the switch in the wall. 

## Pairing with Hubitat

So far, I've been unable to pair these switches with the hubitat controller. I'm not sure if the issue is range, or user error.  I'm going to try and pair something closer to the hub to see if that works (but first I have to get another zigbee device!).

## Would I buy this again?

Not sure yet. I ordered a few more zigbee devices to see if I can extend the range out to the duck house, hopefully that will solve the problem. If I can get it to pair with the hubitat, then yes, I'd likely buy these again. The other issue I've had so far is finding zigbee devices in Canada. I've looked at all these websites:

- amazon.ca
- canadiantire.ca
- walmart.ca (does have zigbee devices, but I'm not familiar with the brand)
- homedepot.ca
- rona.ca
- canac.ca

And the zigbee pickings are slim. There does seem to be a ton of Z-Wave stuff, but I prefer the openess of the zigbee spec.
