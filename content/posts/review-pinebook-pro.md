+++
title = "Review of the Pine64 PINEBOOK Pro"
date = "2020-11-17"
+++ 

I've been wanting to grab a [PINEBOOK Pro](https://pine64.com/product/14%e2%80%b3-pinebook-pro-linux-laptop-ansi-us-keyboard/?v=0446c16e2e66) since I first heard about it a few years ago. The PINEBOOK Pro checks all the boxes I care about when considering hardware: 

- [x] Prioritizes open source
- [x] Runs Linux/BSD/etc
- [x] Small 
- [x] Solid build quality
- [x] Replaceable parts

It's a solid little laptop and is perfect for my use cases: 

- Programming
- Writing blog posts
- SSH'ing to servers to do stuff
- Playing [Minetest](https://www.minetest.net/) with my kids
- Playing around with open source software

## First impressions

The PINEBOOK Pro comes in a non descript cardboard box. It's nice. The laptop looks sleek and feels solid. There is no obvious branding.

## What I like

### Build quality

The build quality is excellent, it feels more solid than my old Dell XPS 13 which cost about 6 times more than the PINEBOOK Pro. The screen is great as well, a 14" 1920x1080 IPS display with excellent brightness that's easy on the eyes. It's also a matte screen which is a nice change from all the glossy displays I've had on my previous several laptops. The CPU is powerful enough to handle the admittedly non-cpu intensive work loads I throw at it: compiling Go code, web browsing, email, writing.  

### Price

$199USD. That's an amazing price point for what you get. 

### Open source

I'm a bit of an open source zealot, and it's nice to have a laptop built by a comapny that cares about being open and supporting open source.

### Battery life

So far the battery life has been great. I can code all day and not have to worry about plugging it in.

## What I don't like

### Trackpad

The trackpad is the main thing that sticks out as needing improvement. I've manage to mostly fix it via the sensitivity settings, but it still seems a bit finiky. I often find myself barely touching it while I'm typing which causes the cursor to jump around and I end up writing where I didn't intend to. There is a firmware update (unofficial) that is supposed to improve the experience, but I haven't tried it yet. 

### Speakers

The speakers are pretty anemic, even at full volume it's hard to hear the sound if you're in a space with background noise.

### Charging

I've noticed a few quirks while charging the laptop. It doesn't seem to charge consistently when the lid is closed and the laptop is suspended. The first day I got it, I closed the lid andplugged it in to charge overnight and when I woke up the battery was dead. I've also noticed that the battery seems to run down pretty quickly when the lid is closed. Not sure if this is an issue with the suspend function, or something else.

## OpenBSD

OpenBSD seems to be well supported on the PINEBOOK Pro, but I haven't had a chance to install it yet. I need to order the UART cable for the laptop first before I can attempt this. [Others have already had success](https://xosc.org/pinebookpro.html). I'll write about it when I get to it here: [OpenBSD on the PINEBOOK Pro](/posts/openbsd-on-the-pinebook-pro.md).

## Would I buy this again?

Yes, 100%. That said, I don't recommend the PINEBOOK Pro unless you're a tinkerer, or hobbyist computer user. There will be some fooling around to get things tweaked after you get it out of the box. It's not quite ready for casual computer users, but it's very close. 
