+++
title = "Software I use"
draft = true
date = "2021-08-04"
+++

A somewhat up to date list of the open source software I use regularly.

## OpenBSD

This is the foundation of all my computing requirements. I use OpenBSD on my
laptop, my servers, everywhere. The only place I can't currently run OpenBSD is
on my work laptop, which is annoying, but I understand.

[OpenBSD](https://www.openbsd.org)

## Firefox

I've been using this browser since it was called Netscape Navigator, and though
I did switch to Chrome for a few years I've since moved back and will likely
stay on Firefox for a long time.

[Firefox](https://https://www.mozilla.org/en-US/firefox/new/)

## Calibre

My e-book organizer/reader of choice, though I don't read many e-books on my
computer, Calibre lets me manage my collection and sync with my kobo e-book
reader.

[Calibre](https://calibre-ebook.com/)

## Vim

Many years ago I typed `vim` at the terminal, and I've been trying to figure out
how to exit ever since. 

[Vim](https://www.vim.org/)

## Thunderbird

Has calendaring, contacts, and email handling out of the box. 

[Thunderbird](https://www.thunderbird.net/en-US/)

## aerc

For when I don't want to leave the terminal to check my mail.

[aerc](https://aerc-mail.org/)

## hugo

Static site generator, what lets me write simple markdown files and turn them
into this website.

[hugo](https://gohugo.io)

## HomeAssistant

All the home automation things.

[homeassistant](https://www.home-assistant.io)

## tmux

Terminal multiplexer, for having a bunch of terminal "tabs" in a single terminal
window, also useful for maintaining state in case of connection loss.

[tmux](https://github.com/tmux/tmux)

## httpd

The http server included as part of OpenBSD, what is currently serving this
webpage.

[httpd](https://man.openbsd.org/httpd.8)

## smtpd

I run my own mail server, and this is the server software I uses to handle
incoming and outgoing SMTP traffic, also included in the OpenBSD base system.

[smtpd](https://man.openbsd.org/smtpd.8)

## git

My version control software of choice. I did have a long running love affair
with mercurial, but the inertia of git has won me over.

[git](https://git-scm.com/)

## draft

One of my own programs, I use it to take notes.

[draft](https://www.gitlab.com/gabeguz/draft)
 
## ecowitt

Another of my own programs, I use it to send ecowitt weather sensor information
to my homeassistant instance.

[ecowitt](https://www.gitlab.com/gabeguz/ecowitt)
