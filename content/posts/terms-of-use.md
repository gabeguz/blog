+++
description = "An attempt at writing some funny terms of use."
categories = [
  "random",
]
tags = [
  "humor",
  "humour",
]
date = "1999-02-06T00:41:48-05:00"
title = "Student Computing Terms of Use"

+++
All Student Computing hardware or software loaned to any department is subject
to the following terms of use:

1. We reserve the right to reclaim said device at any moment in time, past,
present, or future. In order to reclaim said device, the following methods may
be used at our discretion: 
 - Email notification
 - Phone call 
 - Singing Telegram
 - Summoning (note: only certain techs are qualified to perform a summoning)
2. If said device is lost or stolen, it is the sole responsibility of whoever
borrowed the item (henceforth you) to replace with interest the neglected
device. If you choose not to return the device then you may be subjected to the
following penalties, again at the lenders (henceforth us) discretion:
 - Monetary fee (to be determined by a séance of no less than 3 and no more than 5 student
techs) 
 - Lashings. (no less than 12 lashings for any given device, additional
lashings may be sentenced if it is so decreed by an ad-hoc tribunal made up of
at least 2 student techs and one supervisor)
 - Penance (only valid if assigned by a member of the Roman Catholic clergy in good standing with his respective
order)
3. The not covered anywhere else clause: In the case that none of the above
articles apply, Student Computing reserves the right to modify these terms in
any way shape or form both physical as well as astral so that they may best fit
our purpose at any given time. 
