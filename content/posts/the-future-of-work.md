+++
title = "The future of work"
draft = true
+++

The future of work is asynchronous and distributed. 

The past year has shown definitively that remote work is effective. The question
we should be asking ourselves isn't "how will we return to the office after the
pandemic", but "what should we do with all this extra office space?" 

There seems to be a pretty even split between people who want to continue
working from home full time and those who want to go back to the office.
Interestingly, most of the people who want to go back to the office don't want
to go back full time, they want to go back on their own terms w/out having a
requirement to be present at the office. The last survey I saw showed that only
8% of respondents wanted to go back to the way things were before the pandemic.
