+++
description = "Some predictions for the year 2025"
categories = [
  "random",
]
tags = [
  "prophecy",
  "future",
]
date = "2005-09-21T21:36:12-05:00"
title = "The Future"

+++
A friend recently asked for my "expert opinion" on what changes we can expect
to see in technology over the next 20 - 50 years. He is in a writing
competition for some scholarship money, so I decided I would think about the
question without my normal sense of sarcasm towards people who try to predict
the technological future.

In order to answer adequately, I need to make a distinction between the
question "What do I want technology to do", and "What do I think technology
will do" over the course of several decades. The first question evokes a sense
of longing and excitement when I think about it, the second, less so. It's not
that I am cynical of the future, only that I realize that idealized thoughts of
clean energy, and flying cars, are just that, idealizations. In reality, I feel
that there will be some sweeping changes in technology, as well as some
incredible stagnation. Most technology will effectively remain on par with
where it is today, while a small amount will be radically changed.

Some things I imagine we will see in the next 20 years:

- the demise of personal computers as we currently know them (laptops + desktops)
- extreme miniaturization, unheard of performance, and near unlimited storage (for today's needs, the needs of the future will probably be higher) for electronic devices
- no more compact discs (except in niche markets)
- bye, bye film cameras (except in niche markets)
- wireless everything, everywhere
- empowerment of small independent media content creators (films, music, books, etc)
- man on the moon, again

Some things I would LIKE to see in the next 20 years:

- clean energy alternatives become the major source of energy in the US if not the world
- someone comes up with a better, faster way to interact with a computer, other than the silly QWERTY keyboard (DVORAK doesn't count, no one uses it)
- the US actually practices what it preaches, and destroys it's nuclear arsenal
- woman on mars

Some things that I am 90% sure will be the same:

- toasters will still not work consistently, my toast will always be burnt or undercooked
- the entertainment industry will still be attempting to patent away and stifle technological innovation (luckily, it will still be largely unsuccessful)
- people will still be happy using Microsoft crap to get things done
- people will still be using the QWERTY keyboard layout
- no one will have invented a better mousetrap (apparently, it is already perfect)

Granted, this list is a little tongue in cheek, apparently I can't have a
conversation about technology without being a cynic. Naturally, I hope more of
my "Likes" get onto the list of "Wills" but, with technology, you just never
know. In some ways our technology advances at a staggering pace, while in other
ways it remains in the dark ages. Either way, I believe we are living in an
interesting time, and if we can manage to continue existing w/out killing each
other, we have the potential to do great things. 
