+++
description = "SSH bruteforce attempts, and OpenBSD to the rescue"
categories = [
  "tech",
]
tags = [
  "OpenBSD",
  "ssh",
  "pf",
  "firewall",
]
date = "2006-01-01T23:30:34-05:00"
title = "Those Crazy Hackers"

+++

Well, I guess it had to happen eventually. I noticed the following entries in my
`/var/log/authlog` on my cvs server today:

```
Jan 1 17:37:19 mini sshd[22492]: Failed password for root from 218.43.105.210 port 46065 ssh2
Jan 1 17:37:19 mini sshd[25389]: Failed password for root from 218.43.105.210 port 46065 ssh2
Jan 1 17:37:19 mini sshd[22492]: Received disconnect from 218.43.105.210: 11: Bye Bye
Jan 1 17:37:21 mini sshd[1470]: Failed password for root from 218.43.105.210 port 46095 ssh2
Jan 1 17:37:21 mini sshd[25098]: Failed password for root from 218.43.105.210 port 46095 ssh2
Jan 1 17:37:21 mini sshd[1470]: Received disconnect from 218.43.105.210: 11: Bye Bye
Jan 1 17:37:23 mini sshd[17586]: Invalid user jane from 218.43.105.210
Jan 1 17:37:23 mini sshd[1560]: input_userauth_request: invalid user jane
Jan 1 17:37:23 mini sshd[1560]: Failed password for invalid user jane from 218.43.105.210 port 46131 ssh2
Jan 1 17:37:23 mini sshd[17586]: Failed password for invalid user jane from 218.43.105.210 port 46131 ssh2
Jan 1 17:37:23 mini sshd[1560]: Received disconnect from 218.43.105.210: 11: Bye Bye
Jan 1 17:37:25 mini sshd[864]: Invalid user pamela from 218.43.105.210
Jan 1 17:37:25 mini sshd[8752]: input_userauth_request: invalid user pamela
Jan 1 17:37:25 mini sshd[8752]: Failed password for invalid user pamela from 218.43.105.210 port 46159 ssh2
Jan 1 17:37:25 mini sshd[864]: Failed password for invalid user pamela from 218.43.105.210 port 46159 ssh2
Jan 1 17:37:25 mini sshd[8752]: Received disconnect from 218.43.105.210: 11: Bye Bye
Jan 1 17:37:27 mini sshd[6857]: Failed password for root from 218.43.105.210 port 46195 ssh2
Jan 1 17:37:27 mini sshd[1772]: Failed password for root from 218.43.105.210 port 46195 ssh2
Jan 1 17:37:27 mini sshd[6857]: Received disconnect from 218.43.105.210: 11: Bye Bye
```

clearly, it looks like an automated ssh brute force attack on my box
mini.guzman-nunez.com. Very rude of someone. In any case, this at least
motivated me to do something to secure the machine. Wait, am I joking? It is
after all an OpenBSD box which, is already more secure than practically any
other OS out there. So, what could I do to make it even MORE secure?

Well, first of all, I turned off sshd authentication to password attempts, so
the only way in now is by using shared key authentication. This is good,
assuming nothing happens to my key. The second thing I did was to create a pf
ruleset, OpenBSD's built in firewall package. I had only done this a few times
in the past, so I was a bit hazy on the details, luckily, the OpenBSD FAQ came
to my rescue, and I now have a box that is very nicely locked down.

You will notice that all those attacks came very quickly in the space of a few
minutes, so there is now a rule that will blacklist anyone who attempts to open
too many ssh connections in too short a period of time. I am also now filtering
all ports except for sshd and httpd, and an nmap of my box returns very little
useful information:

```
Starting nmap 3.75 ( http://www.insecure.org/nmap/ ) at 2006-01-01 19:53 PST
Initiating SYN Stealth Scan against dsl093-034-161.snd1.dsl.speakeasy.net (66.93.34.161) [1663 ports] at 19:53
Discovered open port 22/tcp on 66.93.34.161
Discovered open port 80/tcp on 66.93.34.161
SYN Stealth Scan Timing: About 23.37% done; ETC: 19:55 (0:01:38 remaining)
The SYN Stealth Scan took 92.61s to scan 1663 total ports.
Warning: OS detection will be MUCH less reliable because we did not find at least 1 open and 1 closed TCP port
For OSScan assuming port 22 is open, 31860 is closed, and neither are firewalled
For OSScan assuming port 22 is open, 31396 is closed, and neither are firewalled
Insufficient responses for TCP sequencing (0), OS detection may be less accurate
For OSScan assuming port 22 is open, 30519 is closed, and neither are firewalled
Insufficient responses for TCP sequencing (0), OS detection may be less accurate
Host dsl093-034-161.snd1.dsl.speakeasy.net (66.93.34.161) appears to be up ... good.
Interesting ports on dsl093-034-161.snd1.dsl.speakeasy.net (66.93.34.161):
(The 1661 ports scanned but not shown below are in state: filtered)
PORT STATE SERVICE
22/tcp open ssh
80/tcp open http
Device type: general purpose
Running (JUST GUESSING) : Linux 1.X (85%)
Aggressive OS guesses: Linux 1.3.20 (x86) (85%)
No exact OS matches for host (test conditions non-ideal).

Nmap run completed -- 1 IP address (1 host up) scanned in 105.243 seconds
```

Hopefully this will effectively stop these attacks in their tracks. My authlog
has already calmed down, and there have been no more login attempts by
unauthorized accounts. This whole process took me about an hour, and really
should have been done when I first setup the box. PF is a very easy tool to
learn and use, and if you have a machine connected directly to the net, it's a
good idea to firewall it.

PF also has NAT capabilities and works quite well as a firewall/gateway for a
home network, though I am not currently using it for that. This implementation
is for a single server connected directly to the internet. 
