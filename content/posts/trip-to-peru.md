+++
description = "A log of my trip to Peru"
date = "2006-11-02T22:25:02-05:00"
title = "Trip to Peru"
categories = [
  "adventure",
]
tags = [
  "travel",
  "peru",
  "cusco",
  "inca trail",
  "machu picchu",
  "hiking",
]

+++

11/16/2006 - Machu Picchu, Peru

Picture of Gabe and Malea hanging out in front of Machu Picchu. Wayna Picchu in
the background. Machu Picchu, Wow.

There isn't much more to be said. Machu Picchu is an absolutely incredible Incan
sanctuary nestled high in the Andes mountains. What a beautiful place. 4 days of
walking is an incredibly small price for the payoff.

Picture of Marcelo, our guide, talking to the group. When we got to Machu
Picchu, Marcelo spent the better part of 2 hours showing us around, explaining
Incan life, and generally just being the great guide that he is. We walked
around the West (I think) side of the ruins checking out various rooms,
courtyards, and temples. After the guided part of the tour was up, we were given
free rein.

There were a variety of options open to us, we could climb Huayna Picchu, a
smaller peak next to Huayna Picchu, explore the greater Machu Picchu complex,
enjoy a beer at the cafe, or head back down to Aguas Calientes and the hot
springs. You would think, that after 4 days of hiking, no one would want to
climb another peak. You would be wrong. Of the 14 people in our group 6 decided
to climb Huayna Picchu, and at least another 4 went up the smaller peak.
Incredible.

Picture of Machu Picchu, with Wayna Picchu in the background Picture of Gabe
(me) with the view of Machu Picchu as seen from the top of Wayna Picchu I was
one of the 6 crazies that wanted to keep hiking, so I headed up Huayna Picchu,
for another view of Machu Picchu, and to see the ruins (yup, still more ruins)
at the top. It was a pretty decent hike, fairly steep, with plenty of very
narrow steps. Not the kind of hike you want to make in the rain. I walked up
about as fast as I could, in an attempt to tire myself out. It worked pretty
well, by the time I got to the top, I was ready for a break.

I hung out at the top for about 20 minutes, just checking out the view and
relaxing. Then, I met up with Thijs (one of my group mates) and we hiked back
down the peak, to the sanctuary. I got lost several times, trying to make my way
back through the ruins to the main entrance so I could meet Malea. It doesn't
look like a confusing place, but there are plenty of twists and turns to get
stuck in, and that's exactly what I did. I finally made it out, and met Malea at
the cafe. We hopped on the bus down to Aguas Calients, and the end of our 4 day
trek to Machu Picchu.

Picture of the train trip back to Cusco We met most of our group at a restaurant
in Aguas Calientes, for one last meal, then headed to the train station so we
could get back to Cusco. It was an excellent trip, all around, and I would
definitely suggest you put this on your short list of things to do the next time
you are bored, or need a vacation. Just make sure you get to Cusco a few days
before you start the trail, you will definitely want some time to acclimate to
the high altitude.  

11/16/2006 - Inca Trail, Peru

Picture of dining tent at 4am Sure enough, at 4am, there was Emilio knocking on
the tents, making sure we were up and ready to go. Breakfast was at 4:30am, and
once again was quite hearty. By 5, we were on the trail, waiting for the new
checkpoint to open so we could make our way up to the Gate of the Sun. Once our
tickets were checked, and we were all counted, we started a brisk (and I do mean
brisk) hike up (and I do mean up) to the ruins. Nope, not Machu Picchu, not yet,
but the Gate of the Sun.

Picture of where Machu Picchu should be, if not for the clouds We made it to the
Gate just in time to see a huge bank of clouds totally covering the place where
Machu Picchu should have been.

Not to be dettered, we sat on our asses and waited patiently for the clouds to
part. Some people even tried blowing, which may or may not have helped (hey, if
a butterfly farting in East LA can cause Bill Gates to lose millions, anything
is possible). Eventually, the clouds did part, and we were treated to our first
views of Machu Picchu. Amazing. Did I say that word had lost some of its punch?
Well, apparently it still had some left because that's the first word that came
to mind, after WOW - that place is huge. Picture of Machu Picchu, from a
distance

We took some pictures, and milled about for a bit over a half an hour, and then
we started the walk down to the famous Incan ruins.  

11/15/2006 - Inca Trail, Peru

Picture of our campsite from the ruins Day three started much the same as day
two, with Emilio waking us up with an offering of Mate de Coca. The difference
today was that it was raining, and had been for much of the night. If you have
ever had to backpack in the rain, you know it can get quite miserable, and no
matter what you do, you end up soaked. Either your raingear breathes too much,
and water gets in, or it doesn't breathe enough, and you end up sweating so much
that you may as well not have any raingear on. With this in mind, I put on my
rain top, and zipped off the legs of my pants, converting them to shorts. I had
decided to live by a fellow backpackers sage adivce "my skin is waterproof
enough."

Picture of Malea and Mike at some impressive ruins Today was, for me, the most
visually stimulating day of the hike. Starting with the ruins I had walked to
the day before, now shrowded in clouds, which offered an excellent vantage of
last nights camp site. If only we could see it, through the clouds. Oh well,
luckily I snapped off a few shots the day before, you know, for posterity.

Picture of Malea standing in a cave entrance After we had climbed to the second
pass, and another set of impressive ruins (these Inca sure knew where to build)
we began our descent into the 'cloud forest.' Wow. Really, just amazing. One of
my mates summed it up well, "I feel like we're in the movie Congo.", and indeed,
every step of the way seemed that unreal. You just don't see things like this on
a day to day basis. To me, it felt more like Indiana Jones and the Raiders of
the Lost Ark where Indy steals the idol from the temple in the jungle, you know:
"Throw me the idol, I throw you the whip", than Congo, but I couldn't help but
agree.

Picture of an Incan staircase I made the comment, at some point today, that if
we got to Machu Picchu, and it wasn't there, I'd be perfectly happy having seen
what I've already seen, and I'd feel perfectly justified in having spent my
$400.00 for this trek already. It's not that I didn't think Machu Picchu would
be amazing, but everything we had seen up to now had already taken the potency
out of that word. Amazing stopped meaning what it had meant, just 2 days ago.

Picture of Incan ruins After continuing through the cloud forest, we made
another gradual ascent, to one last set of impressive ruins (how many times can
I say impressive ruins in one trip) from where we could see our final campsite.
This was a set of terraces, most likely agricultural in nature, but no less
impressive for being just a 'garden.'

Picture of Incan ruins After hanging out in the 'garden' for 45 minutes or so, I
made my descent to the campsite, where hot showers, and a bar (imaine that) just
happened to be waiting. I passed on the shower (hey, I've been filthy for 3 days
now, what's one more) but hit up the bar, and had a beer with my fellow treking
companions, and was introduced to a card game named shithead. It's actually a
fairly simple game, easy to learn, and surprisingly fun to play. I'd go into it
but I'm sure you can probably google it if you really care. It turned out to be
a very nice night, with yet another excellent Peru Treks dinner, and (of course)
Mate de Coca.

Picture of After dinner, Marcelo informed us that we would be waking up at 4am
the next morning to be sure we could get to the 'Gate of the Sun' by 7am, as the
sun was rising. The 'Gate of the Sun' is apparently not the original name of
what was an Incan check point, basically a guard station where anyone coming
into Machu Picchu could be screened, and presumably have any containers of
liquid larger than 4oz taken away from them by the Incan version of the TSA.

I can't speak for the other members of my group, but, I went to bed early.

11/14/2006 - Inca Trail, Peru

Picture of the view from the top of Dead Woman's Pass on the Inca Trail The Top!
13,776ft. Out of breath. But what a view.

Picture of the whole group We stopped for a brief break at the top, took a group
photo, and then started the drop down to the next campsite. On the way down I
was lucky enough to shoot six frames of what Marcelo told me was an eagle, and
even luckier that one ended up being 1/2 way decent. Definitely one of the
highlights of the trip even if the picture is a bit blurry.

Picture of what I was told was osome type of eagle Once we made it to the site,
since I was still feeling a little bounce in my step, I decided to walk up to
the ruins that we would be passing in the morning (about a 25 minute walk from
the campsite), and maybe take a few photos. Naturally, I left my pack in the
tent and just took my camera and a water bottle. What a difference 25 pounds
makes, I practically flew up the trail to the ruins, and it turns out it was a
good idea since the next day was cloudy and rainy. But, again, I'm jumping
ahead.

After walking up and back, I had some time off to relax. I took full advantage
of that time, promptly falling asleep until Emilio woke me up for dinner.

In typical Peru Treks fashion we had another excellent meal followed by, you
guessed it, Mate de Coca.  

11/14/2006 - Inca Trail, Peru

Picture of a waterfall along the Inca TrailThe previous night, Emilio, the #2
guide, had jokingly asked us if we wanted tea brought to our tents as a wake up
call at 6am. Everyone laughed and said "yeah, of course." Turns out, Emilio
wasn't joking. Sure enough, at 6am, there he was outside of the tent, pouring a
hot cup of Mate de Coca for whoever wanted one. These guys don't mess around.
Peru Treks, you can come along anytime I go backpacking.

After we were all awake, and had our personal effects back in our packs, we went
in to have a hearty breakfast. I remember it being hearty, since compared to the
standard Peruvian breakfast (bread, butter, jam, and coffee) it was a feast.
Have I mentioned what an outstanding job these guys did making our lives easy on
the way to Machu Picchu?

Picture of Malea putting on her pack.After breakfast, we hefted our packs, and
began day 2. We had been warned that today would be the most difficult of the 4
days, and it was. Over 3,000 feet of elevation gain to the highest point of the
trek, Warmi Wanusca Pass (Dead Woman's Pass) at 13,776ft. Followed by another
1,000 feet of elevation drop to the second campsite. More than 4,000 feet of
elevation change in 7.5 miles. Not a superhuman feat, especially not for the
porters, but if you've never backpacked before, definitely not a walk in the
park either.

Picture of Dead Woman's Pass After about 30 minutes of uphill walking, porters
started flying past us. Realize, that they still had to clean up after
breakfast, take down all of our tents, and put everything on their backs, and
maybe you will start to appreciate them as I came to over the course of our
walk. Phenomenal. I'm sure the sherpas in the Himalayas are equally if not more
impressive. Is digression a literary device? Can it be used to further along the
storyline even while adding nothing substantial to the material? I'm not sure,
but I am good at it.

Where was I?

Picture of Dead Woman's PassOh yeah, plodding along. We had been lucky as of now
with the weather. It had been nice and overcast, without raining so the walk up
to the pass was not as grueling as I'm sure it could have been. Overall, I found
it quite pleasant. Lots of neat stuff to stop and look at, lots of opportunities
for taking photos, and plenty of chatting with the guides who were always either
at the very front, or the very back of the line. I had some really interesting
conversations with both Marcelo and Emilio as we hiked up toward the pass.
Apparently everyone in Cusco wants to start up a trekking company and offer
tourist packages, and when they found out I was a web developer, well, let's
just say I could probably be employed for a very long time in Cusco.

By the time we stopped for 'tea' (have to love treking with a company owned by a
Brit) we were in sight of the pass, and could see people looking like ants at
the top. Not much further to go, and we could take a group picture and begin the
descent to the next campsite.  

11/13/2006 - Inca Trail, Peru

Picture of a Blue AgavePicture of a Nopal in bloomAs we continued plodding along
that first day we walked through high dessert, the first of the many climatic
(not to be confused with climactic, though there were several of those as well)
zones we would pass through. Cacti, Agave, and a strange parasite that when
ground up makes a natural red dye that doesn't seem to fade accompanied us every
step of the way. This was the least strenuous day of walking. We did about 12km
of walking (7.5 miles) in 5 hours. It was a nice warm up for the 2nd day, but
I'm jumping ahead.

Picture of one of the Peru Treks PortersWhen we stopped for lunch, we were given
a hint as to how amazing the porters really are. We walked into an open area,
where the porters, who practicallly ran this part of the trail, were waiting,
with two lunch tents setup and ready to go. We took off our packs, relaxed for a
bit, and then washed our hands (they had soap, water, and towels at the ready)
and sat down to the first of many delicious meals, followed by the ubiquitous
Mate de Coca.

In the beginning of the day we all introduced ourselves, and promptly forgot who
our companions were. Lunch was the first chance we had to really get aquainted,
and start chatting. Turns out, almost 1/2 of our group was from the UK. All
told, there were 2 people from Belgium, 2 from Switzerland, 2 from Northern
Ireland, 6 from the UK, and 2 from the US (Malea and I).

Picture of a river Valley along the Inca TrailWe all chatted for a bit, relaxed
for a few minutes after lunch, and then hit the trail for the last little bit of
walking. Toward the end of the day, we started to move into a more wooded area,
leaving the cacti, and agave behind. By the time we got to the campsite, once
again the porters had everything ready to go. The tents were all setup, dinner
was in the works, and one of the locals had some beer to sell us. Paradise?
Maybe.

Picture of all 22 of the Porters who accompanied us on our trek, and Marcelo,
the head guide.The head guide, Marcelo, got us all into a group, and had the
porters introduce themselves, stating their name and their age, as well as where
they were from. Almost everyone was from the small communities spaced out around
the Cusco area, and though Marcelo insisted the job of a porter was for the
young, and strong, I know I heard the number 40 mentioned more than once as they
went around giving their ages. These guys were amazing, I don't think I can say
that enough times.

Picture of the campsite on our first night on the Inca TrailAfter introductions,
Marcelo asked if any of us would like to visit a 'local kitchen'. Naturally,
everyone said yes, and so he asked the woman who sold us beer earlier if she
would mind a bunch of tourists poking their heads into her house. She said of
course not, and so we all proceeded to poke. By candlelight, we peered into the
inside of her house (not more than a mud hut, really) and quickly noticed the
Guniea Pigs running around on the floor, eating some lettuce. Guniea Pig, or Cuy
as it is called, is a traditional dish in the highlands of Peru, and these Cuy
were apparently not around just as pets. Talk about fresh food.

After thanking the woman we returned to our camp, had a delicious dinner, some
more Mate de Coca, and promptly fell asleep. An excellent end to the first day
on the Inca Trail.  

11/13/2006 - Cusco, Peru

After returning from Arequipa, it was time to visit Macchu Pichu. There is an
easy way to get to Macchu Pichu, and a not so easy way. The easy way involves a
train, and a bus. The not so easy way also involves a train, and a bus, as well
as 4 days of strenuous walking. Can you guess which way I chose? Naturally, a
four day hike sounded more appealing to me than just a 5 minute walk off of the
bus to the ruins.

Picture of hostal Quorichaksa courtyardThe first day began with a 5am wake up
call. One of the guides met us at our hostel, and sat with us as we finished
some Mate de Coca (Coca leaf tea). His name was Marcelo, and apparently, he was
the head guide on this trip. He walked us to the bus that was waiting to take us
to KM 82, the point where we would begin the 39km trek to Macchu Pichu. When we
walked onto the bus we were greeted by the stares of 12 other very groggy
travelers from who knows where. No one seemed happy to be awake so early.

Picture of the central square in OllantaytamboOur first stop was a tiny town
named Ollantaytambo (say that 5 times fast... or once, for that matter). Aside
from a small square, and some impressive cliff side ruins, there isn't much
there. We stopped for a quick breakfast, and to make any last minute purchases.
Unfortunately, there wasn't time to visit the ruins. After our 45 minutes were
up, we were herded back onto the bus for the remainder of the drive to KM 82.

Picture of the Inca Trail sign at KM 82, PiscacuchoUpon our arrival, we were
briefed on the use of the services of 1/3rd of a porter to help us with some of
our load. Considering the 22 porters joining us would already be carrying all of
the communal gear (tents, cooking equipment, food), and my pack weighed less
that it has on any previous backpacking trip I had been on, I decided to opt out
of those services. Malea followed my lead, a decision I am not sure she ended up
being happy with, but which makes her accomplishment so much more enjoyable (at
least, that's what I'm telling myself). We hefted our packs, and as a group,
started out toward the first control area where our passports were stamped, and
we were allowed onto the famous Inca Trail.  

11/11/2006 - Arequipa, Peru

Picture of the Cathedral in Arequipa, PeruSince I never got around to writing
about Arequipa while I was there, I guess I may as well play catch up for a bit.
Arequipa is a very Spanish town, with colonial style architechture everywhere
you look. It is quite a bit larger than Cusco, though not as touristy. The
layout of the central area is a standard grid, expanding out from the central
Plaza de Armas. The most interesting things Malea and I explored were the Santa
Catalina Monastery, and the gigantic Cathedral in the main square.

The Monastary, a convent for Dominican Nuns, was stunning visually as well as
historically. Apparently the founder of the convent was a wealthy widow (Maria
de Guzman) who had strict requirements (financial) for addmiting new novices.
The sisters who enjoyed convent life, really enjoyed it, as many of them brought
several servants with them. The living quarters were also quite nice,
considering it is a convent, each one having its own kitchen area, and in most
cases multiple rooms. The scenery within the convent was incredible, with
beautiful courtyards, and colorfully decorated streets.

Unfortunately, for the sisters, the pope got wind of the goings on at this
monastery and sent in an enforcer. The new Mother Superior ended the lavish
parties, and stopped the sisters from inviting musicians and entertainers from
coming to the convent. She also required that all the servants be set free, many
of whom promptly became nuns. She closed the doors of the convent, and it
remained sealed until 1970 when it opened as a tourist attraction. One area
remained closed and a contingent of 30 nuns continues to live there in
seclusion.

Picture of Malea walking through some of the colorful walls at the Sta. Catalina
Convent in Arequipa, Peru.As it turns out, the Convent also happens to be an
excellent place to play around with a camera, and Malea took full advantage of
my knowledge. She kept asking me what settings I would use to shoot a particular
scene, and so I started to talk to her about depth of field, exposure,
composition, and lighting. Of course reminding her that in photography, there
are no hard rules. She was full of questions which I happily aswered (to the
best of my ability) and together we shot well over 300 frames. Luckily the
convent is a very photogenic place.

The Cathedral in the Plaza de Armas is gigantic. Taking up one complete side of
the Plaza, it looks more like a government building than a church. The plaza is
much larger than the one is Cusco, though I felt much more at home in the
smaller setting than in Arequipa. Something about the square just didn't feel
quite right to me. Perhaps it was the architectural style of the Cathedral. Who
knows. In any case, the Cathedral was immense, and impressive on the inside as
well as the out.  

11/10/2006 - Cusco, Peru

Well, I've been in Peru for just about a week now, and so far It has been a nice
and relaxing trip. It's nice to be back in Latin America, away from the go, go,
go, feeling that seems to pervade life in the U.S. So far, I've only visited two
cities here in Peru, Cusco and Arequipa.

Picture of the Plaza de Armas in Cusco, Peru.Cusco is a smaller city, nestled in
the Andes mountains. At right around 10,975ft the city itself is higher than
some of the peaks in the Lake Tahoe area. I didn't think the altitude would give
me too much trouble since I was already living at 7,000ft but apparently, those
4000 extra feet make quite a difference, and I've been huffing and puffing since
I got here. The weather is also quite chilly up here, and since we are in the
middle of the rainy season, we get a bit of rain every day. Luckily, I love the
rain.

The Spanish colonial influence is evidenent everywhere you look in Cusco.
Coutyards, churches, and plazas most of them constructed using stone that at one
point in time made up the temples, homes, and buildings of the Incan empire. The
city has a very quaint feel to it, and it is very easy to imagine yourself
living in the 16th century. Aside from the cars that now crowd the tiny streets,
little has changed.

Picture of the Valley of Cusco, PeruI spent my first few days in Cusco trying to
get lost. It may sound easy to do, but I found it practically impossible. The
old city expands outward from the Plaza de Armas, and from virtually every
vantage, you can quickly tell which direction to go in order to find yourself
back at the central cathedral. After two days of exploring, I had a very good
feel for the city, even given the lack of consistency with street names. Street
names would change from block to block, sometimes a street would have a sign on
each side, with two differenet names.

One of the nice side effects of my wanderings was that after about 1km of
walking in any direction from the Plaza de Armas, the number of tourists fell
off exponentially, and I soon found myself in the local areas of town. I started
to see places where you could get a full lunch for 2.5 Soles, about 80 cents. As
a comparison, most places in the Plaza sold lunch for about 15 Soles, about
$4.75. I'm not complaining, the food is definitely worth the small price, it is
just an interesting comparison to make.

On the subject of food (be careful, I can write for hours about food), there are
hundreds of delicious places to eat in Cusco. Traditional food, international
food, international food with a traditional twist, traditional food with an
international twist, street food, etc. whatever you want, you can find. I even
saw a few places with grilled cheese sandwiches, so I know even Daniel (my
brother) could eat here.

Picture of Tandapata Street, on the upper side of the Plaza San Blas in Cusco,
PeruSo far, of the places I have tried, a Crepes restaurant called Velluto,
owned by a very nice Peruvian woman named Gisela, has been my favorite. It's not
traditional Peruvian food, but it is delicious, and the crepes they make there
are excellent. Gisela is originally from Lima, but has been living in Cusco for
the past 4 years, sucked in by it's siren call.

I can definitely feel that call myself, Cusco is the type of place you could
easily spend years in. I've met several people here who travel to Bolivia once
every 3 months so that they can get their 90 day tourist visa renewed and stay
in Cusco just a little bit longer.

