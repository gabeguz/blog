+++
date = "2001-03-01T00:22:53-05:00"
title = "Ultimate Frisbee"
description = "Wherein a bunch of nerds attempt to be athletic."
categories = [
  "random",
]
tags = [
  "sports",
  "frisbee",
  "humor",
  "humour",
]

+++

Ok, somehow I was tricked into joining an intramural team at USD. I'm not
exactly sure how it happend, but I do know that Dillon was definitely involved
and may have used some strange mind trick that I wasn't anticipating to get me
to play. In any case, I'm on the Student Computing Ultimate Frisbee team, and
since I'm here I decided I might as well share my suffering with the world. At
least, all two of the people who visit this page, me and... ok, so there's only
one person who comes here stop rubbing it in. The scoreboard below shows our
progress in each game and if you click on the date you will get to read the
story associated with each game.

Ultimate Scoreboard
===================

3/1/2001 - 0 to 24

Unfortunately our first game suffered from a total media blackout and so there
is nothing for me to report to you. Now that the world knows us there will be
much more press. 

3/8/2001 - 0 to 23

After only two games of league play team "Student Computing" maintains it's
near flawless record of never being feated (when someone beats you you are
defeated and so we have never been feated because we've never won), quite an
acomplishment for such a young team. "This game was really a milestone for us
since we drafted Chris just the other day." Coach McIntosh commented
when she was interviewed for this article. "I really think he will add a level
of dynamism we've reached for but never quite attained." Also joining the line
up this week were Juan aka "The QTVR Man" who showed off some of his
killer D. Team captain Dillon recruited Juan after seeing some of his QTVR work
on campus "We could use someone with your eye for detail" he told Juan moments
before the game. Long time veteran Katrina "Get yo frizbe outta my house", one
of the cogs of this well oiled machine, laid the "smack down" in the endzone
preventing the other team from scoring. Tag team Tyler "I'm with the coach"
 and Andy "I go to UNI" showed off their talent in the short
game. "I've seen short passes before," Coach McIntosh said, "but those two
redefine the phrase." No article about this team would be complete without
metioning Tim "The Tool Man" (ooops, wrong show), the very glue that keeps us
together. "Without Tim's encouragement and yelling most of those guys (read
Gabe) would just sit down on the grass and cry." Of course, team captain Dillon
"I made Gabe play and now he's having fun" brought his mastery of Physics and
the force to bear against the other team, which held them down to a sheer 23
points. "I wonder what would have happend without him there tonight" Coach
McIntosh thought to herself later that night(1). In a coup de grace the Coach
even took a turn on the field in an attempt to "Show you youngins how to flip
the disc in the game of Ultimate." Coach McIntosh and family also brought
nourishment for the post game celebration. All in all it was quite a night for
team Student Computing.

For USD-SC NEWS, this is Gabe Guzman, signing off.

(1) This reporter has a form of ESP which allows him to pull quotes out of
peoples heads even when they don't know they ever said what he attributes to
them.

PS. This article is intended to be humorous and in no way should be read as
"making fun" or "cutting down" any of the team members mentioned in the piece.
If you feel I have "hurt your feelings" in any way, please direct you criticism
to Kathy so that she may fire me and ban me from journalism for all of
eternity. 

3/15/2001 - 0 to 23

In loving memory, Wes The Fish. 2000-2001.

Disclaimer: The author makes no claim that this article or any following
article will attain the "laugh ratio" that it's predecesor, article the first,
acheived.

After another stunning feation, team Student Computing maintains its hold for
the number one spot. In a surprise move, coach McIntosh pulls QTVR Jon, aka
"Juan" from her lineup. When asked to comment on this unorthodox move she
replied, "I feel that it's important to dominate this game, not only on a
physical level, but on the psychological level as well. I pulled Juan out in
order to 'psyche out' our opponents, and I'm confident that my tactics were
effective, I heard them wondering where Juan was during the halftime break."
Coach McIntosh assured this reporter that she would be bringing Juan back for
what she likes to call "Pound 'em in the playoffs" a secret tactic her team has
been perfecting these last few games. On the lighter side of things, longtime
player Elizabeth is off the DL (disabled list for all you non sports people)
and playing like a champ once again. "It's a good thing too," the coach
commented, "we really need her distraction tactics, when that girl cartwheels,
people stop and watch." Elizabeth "I'm good at just about everything" turned
red when she heard her new nickname and refused to comment. "It's not my fault"
she mumbled as I moved on. Rumor has it that her and the coach are working on a
way to incorporate her Piano, Ice Skating, and Yo-Yo skill into "Pound 'em in
the playoffs." Back to the game, Katrina "I can pass too ya know" nailed a
beautiful pass to the endzone that Gabe "I've got cleats on, what's my excuse
now?" nearly nailed for the teams 'almost' first point, "I'm sure I
probably caught that in some alternate dimension." Gabe growled as he sulked
back to the sidelines. Rookie turned veteran Chris had a killer game
and managed to get open no matter what. "If that kid were a hammer, he'd hammer
in the morning, and if he weren't our star reciever, we'd get hammered later in
the day" Coach McIntosh stated. In a totally unprecedented move, Carrie "Your
in my way, and i'm NOT stopping" breaks her vow to never play Frisbee
again and comes out of retirement, after a brief stint playing baseball (not
too well, I might add), in order to "Open up a can o' Whoop A#%," which she did
as only Carrie can. Accompanying Carrie was the ever silent Roger "I don't say
much" (I don't know his last name) who said not a whole lot when I questioned
him after the game. "The other team needs to realize that you gotta be wary of
those strong silent types. As long as they don't figure that out, we're still
ahead on the Psychcological battlefield" the coach informed me. Tim "the long
bomb" nailed some 'suuweeet' passes down the field. "I was very proud of Tim,
it's good to see him getting over his fear of hurting others enough to just
launch that thing down the field, now we just have to work on actually having
recievers there to catch it." Said team captain Dillon "I think I'll sit most
of this one out cause Josh needs to talk to me about the playoffs". Tyler "your
spin throw is no match for my high kick spinaround super pass" 
continually foiled the other team's defenders by finding the open guy/girl and
getting her/him the frisbee. "Their Kung-Fu is not as good as mine" he was
heard repeating throughout the game, and he was right, Tylers Kung-Fu held them
down again, to only a measly 23 points after two halfs of tournament play.
Carrie "I was retired kinda like Michael Jordan" was heard commenting "I
kinda like that number, Twenty Three, see how it just sorta rolls off your
tounge?" No, Carrie, I don't, but then again, I'm just a reporter.

Signing off, this is gabe guzman, with another SC Sports News Brief.

PS. Have a wonderful break everyone and to everyone a wonderful break. 

4/5/2001 2, count em 2 POINTS!! - 20

April 5, 2001. A night that shall be remembered long after we are all gone. A
night that shall be recorded in the annals of time and sung round the fire in
years to come. For it was on this night that the impossible was attained, that
a true miracle occurred on the valley field at the University of San Diego.
Were the dead raised? Were the sick healed? Were the blind made to see? All of
these pale in comparison to the events of this night. At precisely 37 minutes
past the eighth hour two members of Team Student Computing changed history
forever. Timothy and Elizabeth completed the first successful pass into an end
zone that this team has ever seen. Not only did they score on an eleven point
deficit, but they rallied and scored again on their very next possession. Two
points, one unanswered, back to back in less than three minutes of official
game play. In this hour of need, the whole team came together as one unit for
the very first time. Andy effectively razzled the other team with his
booming voice and random gibberish, Dillon showed us what team captains are
made of as he lead the counter offensive. Coach McIntosh played after a
dentist appointment saying "No matter what, this is the last game, and we're
not going out without scoring again." Katrina, with her triathlete endurance
had more game time than any other player and made defensive plays that easily
rival her past performances. She also showed off her offensive moves with some
amazing passes and completions, helping move the disc close enought for Tim and
Elizabeth to score. Newcomer Joey Joe Joe Junior Shabadoo (Joe) did an
amazing job for being his first game, with enough catches to name him most
improved player this season. Tyler, once again, a key driving force in
this team helped us all coordinate our efforts, and pulled us out of a few jams
with his accurate passing. Even Harmonica John deserved mention as he egged us
on from the sidelines and delivered some nice long passes toward the goal. Even
though only two people scored, this was a combined effort, and all of you
deserve the highest honors for this noble performance. Team Student Computing
thanks you, it's members, for making this night the standard by which all
future games will be measured. Team Student Computing, I salute you.

This article was written in memory of our teammates that could not join us for tonight's game, and for Wes the fish.

Carrie
Rog, Carrie's boyfriend
Chris
Juan
Kristen
Heather
Gor

Brought to you by the letters Q and A, and by the number 2. 

WE ARE GOING TO THE PLAYOFFS!

Well, we made the playoffs, and we scored! Two huge points. We rule!
