+++
title = "Voltron, defender of the universe"
draft = true
+++

When I was a kid, I used to be a fan of Voltron. I didn't own all five lions,
but I had the black lion and I'd watch the show anytime it came on. What a nice
surprise it was to see that Netflix has a new version of Voltron with 8 seasons!
I started watching it with my kids and we're really enjoying it. 

