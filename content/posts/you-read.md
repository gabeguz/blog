+++
categories = [
  "reading",
]
tags = [
  "books",
  "reading",
]
date = "1999-12-08T00:10:01-05:00"
title = "you, read."
description = "A list of books I liked, circa 1999."

+++
A few of my friends have convinced me that I should have a 'recommended reading' page since I've read so many books. Well, this is it. On this page I will list the books that have had an impact on me and have somehow shaped my life. The books aren't listed in any particular order, and sometimes I might write a quick blurb to describe why a given book is on my list.
Here are some other book lists that you may want to look at:
Karen's Sci-Fi Reading List

- The Chronicles of Narnia C.S. Lewis - This is perhaps the series of books that started it all. I read these for the first time in 3rd grade and have been in love with reading ever since. If you only read one of my recommendations, make sure this it it.
- The Trilogy - If I have to tell you which trilogy, or who wrote it, then you probably shouldn't even bother reading it. Just kidding, The Lord of the Rings J.R.R. Tolkien.
- The Stand Stephen King - Read this book, seriously.
- The Princess Bride William Goldman - If you loved the movie, then you should definitely read this book, especially the introduction. It adds a depth that the movie doesn't have time to go into.
- The Source James Michener - Great historical fiction. A definite must read for anyone planning on visiting Israel.
- The Hitchhiker's Guide to the Galaxy Douglas Adams - If you ever expect to survive in this Universe, you need the answer to the question of life the universe and everything, and only this series can provide it.
- Tuesday's with Morrie Mitch Albom - Not a book that I would have read, but a good friend of mine told me I had to, and I'm glad I did. Great book, very emotionally charged.
- The Chosen Chaim Potok
- The Chronicles of Thomas Covenant the Unbeliever Stephen R. Donaldson - I always felt these books started off slow, but after the first 70 pages, I couldn't put them down. This was another series that got me really into reading fantasy novels.
- The Dark Tower Stephen King - This is a great series of books, my only complaint is that the next one never comes out until you've forgotten everything that has happened in the previous one.
- 1984 George Orwell - Excellent. Read this.
- The Wheel of Time Robert Jordan - Another great fantasy series. Nine books so far, and still growing.
- Fahrenheit 451 Ray Bradbury - Ohh, such a good book.
- The Sword of Shannara Terry Brooks - This is the first book in what is now an eight book series. Terry Brooks writes amazing fantasy novels, and the Shannara series is by far my favorite of his works.
- Wizard's First Rule Terry Goodkind - This is book one of "The Sword of Truth" another excellent fantasy series. People complain that is is too similar to Robert Jordan's Wheel of Time, but that's a bunch of crap. It's very entertaining reading, and that's what is important.
- Cat's Cradle Kurt Vonnegut - This is a pretty quick read, and a really good one too. Plus it's not a fantasy series, so that should make some of you out there happy.
- Into the Wild John Krakauer - This is another one of those books that I wouldn't have normally read, but a friend said I should, and it was great. True story, very powerful, a great read.
- Ender's Game Orson Scott Card - I just recently read this book, and it was excellent. Science Fiction, but even if you don't like that genre you should read this book.
- Catch 22 Joseph Heller - I haven't enjoyed a book this much in a long time. A definite must read.
- Atlas Shrugged Ayn Rand - This book was excellent. I can't say enough good things. Everyone should read this book at some time in their lives. Not only is it an amazing read, but it is a philosophical work as well. One of my favorite books.
- Snow Crash Neal Stephenson - A really interesting perspective on the future. Sci-Fi, if you consider yourself a computer nerd, you need to read this. Else, you should probably read it anyway. It's good.
- Jude the Obscure Thomas Hardy - Depressing? Yes. Wonderful? Yes. A must read? Definitely. Made me cry.
- Cryptonomicon Neal Stephenson - Great book.


Books I'd like to read
======================
Current Book List (exactly as it appears on the piece of paper that's in my wallet)

- Longitude - Dana Sobel
- American Ceaser - ?
- Into Thin Air - ?
- El club Dumas - Arturo Pérez Reverte
- The Edge of the Bed: How Dirty Pictures Changed My Life - Lisa Palac
- Pillars of the Earth - Ken Follet
- The Drifters - James Michener
- Finnegans Wake - James Joyce
- Anna Karenina - Leo Tolstoy
- The Story of B - Daniel Quinn 

That's all for now. I'll add a few more books every week or so.
-gabe. 
