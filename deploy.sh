#!/bin/sh
USER=root
HOST=www.lifewaza.com             
DIR=/var/www/htdocs/www.lifewaza.com/   # the directory where your web site files should go

hugo && rsync -rlptDvz --delete public/ ${USER}@${HOST}:${DIR}

exit 0
